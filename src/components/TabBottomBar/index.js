import React from 'react';
import { View,Text, StyleSheet, TouchableWithoutFeedback, Platform } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { NavigationActions, StackActions } from 'react-navigation';
import { navigatorRef } from "../../App";


const styles = StyleSheet.create({
  tabbar: {
    ...ifIphoneX({
      height: 80,
      paddingBottom: 20
  }, {
    height: 60
  }),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.greenmosslight,
  },
  tab: {
    alignSelf: 'stretch',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5
  },
  textBar: {
    textAlign: 'center',
    fontSize: Platform.OS == 'ios'? metrics.fontTabBarIos : metrics.fontTabBar,
    color: colors.white,
    marginTop: 5
  }
});

class TabBar extends React.Component {

  constructor(props) {
      super(props);
  }

  jump(key){
    const nav = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'MainRoute',
          params: {},
          action: NavigationActions.navigate({ routeName: key, params: {} }),
        }),
      ],
      key: null
    });

    const {
      navigation,
      renderIcon,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      jumpTo
    } = this.props;
    navigation.goBack(null);
    jumpTo(key)
  }

  render() {
    //console.log(this.props)
    const {
      navigation,
      renderIcon,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      jumpTo
    } = this.props;

    

    const {
      routes
    } = navigation.state;


    return (
      <View style={styles.tabbar}>
        {
            routes && routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            return (
              <TouchableWithoutFeedback
                key={route.key}
                onPressIn={() => route.key == "Menulateral" ? navigation.openDrawer() : this.jump(route.key)  }
                style={styles.tab}
              >
                <View style={styles.tab}>
                  {
                    renderIcon({
                    route,
                    index,
                    focused,
                    tintColor
                  })}
                  <Text style={styles.textBar}>{getLabelText({route: route})}</Text>
                </View>
              </TouchableWithoutFeedback>
            );
          })}

      </View>
    );
  }
};

export default TabBar;
