import React, { Component } from 'react';
import { Linking,Text, View, ImageBackground, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { navigatorRef } from "../../App";
import styles from './styles';
import colors from '../../styles/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-navigation';

//redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as AssociadosActions } from '../../store/ducks/associado';
import general from '../../config/general';

class DrawerSidebar extends Component {
  logout() {
    const { navigation } = this.props;

    AsyncStorage.removeItem('UserData', () => {
      const nav = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'LoginStack'
          })
        ],
        key: null
      });
      navigatorRef.dispatch(nav);
    })
  }

  componentDidMount = async () => {
    let token = await AsyncStorage.getItem("UserData");
    this.props.getAssociadoRequest(token)
  }

  render() {


    const { navigation } = this.props;

    var notificacoes = this.props.notificacoes.data
    var total_nao_lidas = notificacoes.filter((obj) => obj.Lido === false).length;

    return (
      <SafeAreaView style={{flex:1, backgroundColor: colors.greenmosslight}} forceInset={{ bottom: 'never' }}>
        <View style={styles.bgFundo}>
          
          <TouchableOpacity style={styles.txtClose} onPress={() => navigation.closeDrawer()}>
            <Text >
              <Icon name="times" color="#fff" style={styles.iconClose} />
            </Text>
          </TouchableOpacity>

          <View style={styles.headerUsuario}>
            <ImageBackground style={{ width: '100%', height: '100%' }}
              resizeMode='cover'
              source={require('../../assets/bgMenuSidebar.png')}>

              <View style={styles.containerUsuario}>

                {
                  this.props.associado.data.Foto === null && this.props.associado.data.CaminhoFoto === '' ?
                    <Image
                      source={require('../../assets/perfilpadrao.png')}
                      style={
                        styles.imgUsuario
                      }
                    /> :
                        this.props.associado.data.CaminhoFoto != '' ?
                          <Image
                            source={{ uri: this.props.associado.data.CaminhoFoto }}
                            style={
                              styles.imgUsuario
                            }
                          /> :
                          <Image
                            source={{ uri: general.imagemPerfil + this.props.associado.data.Foto }}
                            style={
                              styles.imgUsuario
                            }
                      />
                }


                <Text style={styles.nomeUsuario}>{String(this.props.associado.data.Nome).toUpperCase()}</Text>
                <Text style={styles.txtPontos}>
                  Você possui<Text style={styles.Pontos}> {this.props.associado.data.SaldoPontos} pts</Text>
                </Text>
                <Text style={styles.txtPontos}>
                  {String(this.props.associado.data.Categoria ?.Nome).toUpperCase()}
                </Text>
                <View style={styles.containerBtns}>
                  <TouchableOpacity activeOpacity={0.8} style={styles.btns} onPress={() => { navigation.navigate('meuPerfil') }}>
                    <Text style={styles.txtBtns}>MEU PERFIL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={0.8} style={styles.btns} onPress={() => { this.logout() }}>
                    <Text style={styles.txtBtns}>SAIR</Text>
                  </TouchableOpacity>
                </View>

              </View>

            </ImageBackground>
          </View>
          <View style={styles.listMenu}>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('NotificacaoStack') }}>
              <View style={[styles.menuTxtBtns, styles.menuBtnsNotificacoes]}><Text style={{color:colors.black}}>Notificações</Text>
                {
                  total_nao_lidas > 0 ?
                    <View style={styles.alert}>
                      <Text style={styles.alertNumber}>{total_nao_lidas}</Text>
                    </View>
                    : null
                }
              </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('Lojas') }}>
              <Text style={styles.menuTxtBtns}>Lojas e Benefícios</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('Inicial') }}>
              <Text style={styles.menuTxtBtns}>O Conexão Eldorado</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('regulamento') }} >
              <Text style={styles.menuTxtBtns}>Regulamento</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('DuvidasFrequentesStack') }}>
              <Text style={styles.menuTxtBtns}>FAQ</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.3} style={styles.menuBtns} onPress={() => { this.props.navigation.closeDrawer(); navigation.navigate('HistoricoFaleconoscoStack') }}>
              <Text style={styles.menuTxtBtns}>Fale Conosco</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={styles.viewSocial}>
            <TouchableOpacity activeOpacity={0.8} style={styles.btnSocial} 
              onPress={()=> Linking.openURL("https://twitter.com/aapp_oficial")}
            >
              <Image
                source={require('../../assets/icon-twitter.png')}
                style={
                  styles.imgTwitter
                }
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} style={styles.btnSocial}
            onPress={()=> Linking.openURL("https://www.facebook.com/PontePretaOficial")}
            >
              <Image
                source={require('../../assets/icon-facebook.png')}
                style={
                  styles.imgFace
                }
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} style={styles.btnSocial}
            onPress={()=> Linking.openURL("https://www.instagram.com/pontepretaoficial/")}
            >
              <Image
                source={require('../../assets/icon-instagram.png')}
                style={
                  styles.imgInstagram
                } 
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} style={styles.btnSocial}
            onPress={()=> Linking.openURL("https://www.youtube.com/user/tvponte?sub_confirmation=1")}
            >
              <Image
                source={require('../../assets/icon-youtube.png')}
                style={
                  styles.imgYoutube
                }
              />
            </TouchableOpacity>
          </View> */}
      </View>
      </SafeAreaView>
  );
  }
}
const mapStateToProps = state => ({
  associado: state.associado,
  notificacoes: state.notificacoes
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...AssociadosActions,
  }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(DrawerSidebar);

