import {  StyleSheet, Platform } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { Right } from 'native-base';
import { ifIphoneX } from 'react-native-iphone-x-helper'


const styles = StyleSheet.create({
    header: {
        ...ifIphoneX({
            paddingTop: 50,
        },{
            paddingTop: Platform.OS == 'ios' ? 20 : 0
        }),
        width: '100%',
        backgroundColor: colors.greenmosslight,
        textAlign: 'center'
    },
    icoMedal: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    container: {
        width: metrics.defaultWidthPag,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: 'space-between'
    },
    dados: {
        flexDirection: 'row',   
        flexWrap: 'wrap',     
        justifyContent: 'center',
        width: 250
    },
    nome: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: metrics.defaultFontSizeTxt,
        width: '100%',
        textAlign: 'center',
    },
    txtPts: {
        color: colors.greenmoss,
        fontSize: 13,
        width: '100%',
        textAlign: 'center'
    },
    pts: {
        color: colors.greenmoss
    },
    notificacoes: {
        position: 'relative',
        marginRight: 6
    },
    alert: {
        position: 'absolute',
        backgroundColor: colors.greenmoss,
        top: -4,
        right: -4,
        borderRadius: 5,
        textAlign: 'center',
        width: 11,
        height: 11,
        justifyContent: 'center'
    },
    alertNumber: {
        fontSize: 8,
        color: colors.white,
        textAlign: 'center'
    },
    icoNotificacoes: {
        width: 23,
        height: 23
    }

});

export default styles;
