import {
  StyleSheet, Platform
} from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';


const styles = StyleSheet.create({
  containerFull:{
    backgroundColor: colors.white
  },
  header: {
    paddingTop: Platform.OS == 'ios' ? 15 : 0,
    width: '100%',
    backgroundColor: colors.greenmosslight,
    textAlign: 'center'
  },
  containerHeader: {
    width: metrics.defaultWidthPag,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'space-between'
  },
  title: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: 'center',
    width: '85%',
    paddingRight: 30
  },
  btnIconBack: {
    height: 45,
    width: '15%',
  },
  iconBack: {
    color: colors.white,
    fontSize: 21,
    lineHeight: 45
  },
  menu: {
    width: '100%',
    backgroundColor: '#000000',
    paddingTop: 25,
    paddingBottom: 25,
  },
  container: {
    width: metrics.defaultWidthPag,
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    flexWrap: 'wrap'
  },
  containerPerfil: {
    width: metrics.defaultWidthPag,
    justifyContent: 'flex-start',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  btnDestaque: {
    width: '50%',
    backgroundColor: '#B9B9B9',
    minHeight: 25,
    borderRadius: 50
  },
  btnSemDestaque: {
    width: '50%',
    minHeight: 25
  },
  txtDestaque: {
    width: '100%',
    color: '#ffffff',
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: 'center',
    lineHeight: 25,
    fontWeight: 'bold'
  },
  txtSemDestaque: {
    width: '100%',
    color: '#B9B9B9',
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: 'center',
    lineHeight: 25,
    fontWeight: 'bold'
  },
  imageUser: {
    width: 109,
    height: 109,
    position: 'relative',
    marginTop: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 30,
  },
  imageItem: {
    width: 109,
    height: 109,
    position: 'absolute'
  },
  altImg: {
    backgroundColor: colors.gray,
    height: 30,
    width: 30,
    lineHeight: 30,
    textAlign: 'center',
    borderRadius: 20,
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  altIco: {
    color: colors.yellow,
    height: 30,
    width: 30,
    lineHeight: 30,
    textAlign: 'center',
  },
  form: {
    width: '100%'
  },
  camposInput: {
    width: "100%",
    borderBottomWidth: 1,
    color: '#2F2F2F',
    borderBottomColor: colors.graylight,
    paddingLeft: 25,
    marginTop: Platform.OS == 'ios' ? -15 : -32,
    paddingBottom: Platform.OS == 'ios' ? 10 : 0 
  },
  camposSelect: {
    width: "100%",
    borderBottomWidth: 1,
    color: '#2F2F2F',
    borderBottomColor: colors.graylight,
    paddingLeft: 25,
    marginTop: -32,
    height: 50,
    width: '100%',
    transform: [{
        scaleX: 0.9
      },
      {
        scaleY: 0.9
      },
    ]
  },
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: '#cf9112',
    width: 280,
    height: 35,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30
  },
  buttonContainer: {
    marginTop: Platform.OS == 'ios' ? 20 : 0,
    width: '100%',
    flexDirection: "row",
    justifyContent: "center"
  },
  txtButton: {
    color: '#fff',
    fontSize: metrics.defaultFontSizeTxt,
    fontWeight: 'bold'
  },
  cancelAc: {
    fontSize: metrics.defaultFontSizeTxt,
    color: colors.gray,
    textAlign: 'center',
    marginBottom: 30,
    marginTop: 10,
    width: '100%'
  },
  cancelAcBold: {
    fontWeight: 'bold'
  },
  titleArea: {
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: metrics.defaultTextAlignTitle,
    fontWeight: metrics.defaultFontWeightTitle,
    marginTop: metrics.defaultMarginTopTitle,
    marginBottom: metrics.defaultMarginBottomTitle,
    color: colors.gray
  },

  imgBackground: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerModal: {
    width: metrics.defaultWidthModal,
    backgroundColor: '#ffffff',
    padding: 20,
    borderRadius: 5
  },
  containerModal2: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    width: metrics.defaultWidthModal,
    backgroundColor: '#ffffff',
    padding: 20,
    borderRadius: 5
  },
  btnFechar: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '80%',
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  iconSuccess: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10
  },
  iconTimes: {
    width: '100%',
    textAlign: 'right',
    position: 'absolute',
    top: 10,
    color: colors.yellow,
    fontWeight: 'bold',
    paddingRight: 20
  },
  allModal: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtBtns: {
    color: '#fff',
    fontFamily: 'Open Sans',
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: "center",
  },
  btnFechar: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '80%',
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  iconSuccess: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10
  },
  iconAlert: {
    fontSize: 45,
    marginTop: 30,
    color: '#000000'
  },
  iconTimes: {
    width: '100%',
    textAlign: 'right',
    position: 'absolute',
    top: 10,
    color: colors.yellow,
    fontWeight: 'bold',
    paddingRight: 20
  },
  allModal: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtBtns: {
    width: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#ffffff',
    lineHeight: 35
  },
  btnNo: {
    height: 35,
    width: '40%',
    borderRadius: 50,
    backgroundColor: colors.yellow,
    marginTop: 15
  },
  btnYes: {
    height: 35,
    width: '40%',
    backgroundColor: '#019e35',
    borderRadius: 50,
    marginTop: 15
  },
  containerBtns: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textModalAvise: {
    marginTop: 15,
    marginBottom: 10
  }
});

export default styles;