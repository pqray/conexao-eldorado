
import React from 'react';
import { TouchableOpacity, Platform, Text, View, Image, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import images from '../../styles/images';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Alert from '../../components/Alert';
//redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ForgotActions } from '../../store/ducks/forgot';

import Loader from '../../components/loading';
import { ifIphoneX } from 'react-native-iphone-x-helper';

class esqueciSenha extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      text: 'Useless Placeholder',
      Email: '',
      modalVisible: false
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  validation = () => {
    if (!this.state.Email) {
      return false;
    }
    return true
  }

  submitForgot = () => {
    if (!this.props.forgot.loading)
    {
      let mensagem = this.validation()
      if (mensagem == true) {
        console.log("Executou");
        this.props.getForgotRequest(this.state);
      } else {
        this.setModalVisible(true);
        
      }
    }
  }

  render() {
    console.log(this.props.forgot,"forgot props")

    const { goBack } = this.props.navigation;
    return (
        
          <KeyboardAwareScrollView contentContainerStyle={styles.container}> 
            <Loader
              loading={this.props.forgot.loading}
            />
            <Image
              source={images.logo}
              style={
                styles.logo
              }
                />
                
            <Text style={styles.txtEsquecisenha}>ESQUECI MINHA SENHA</Text>

            <Text style={styles.txtDescricao}>Digite o e-mail cadastrado. Você receberá um link para redefinir sua senha.</Text>

            <View style={{ width: '85%', marginTop: Platform.OS == 'ios'? 30 : 10 , ...ifIphoneX({ marginTop: -25, })}}>
              <Icon name="envelope" style={styles.iconEnvelope}/>

              <TextInput
                style={styles.camposInput}
                returnKeyType="go"
                onSubmitEditing={() => this.submitForgot()}
                autoCapitalize="none"
                placeholder="e-mail profissional"
                placeholderTextColor="#2F2F2F"
                onChangeText={key => { this.setState({ Email: key }) }}
                value={this.state.Email}
              />
            </View>

            <View style={styles.buttonContainer}>
              
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.buttonEntrar}
                onPress={() => { this.submitForgot() }}>
                <Text style={styles.txtButton}> ENVIAR E-MAIL </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity activeOpacity={0.8} style={styles.footer} onPress={() => goBack()}>
              <Text style={styles.txtFooter}>VOLTAR A TELA DE <Text style={styles.boldTextStyle}> LOGIN</Text>
              </Text>
            </TouchableOpacity>    
            
            <Alert 
            visible={this.state.modalVisible}
            icon="exclamation"
            mensagem={Platform.OS == 'ios' ? 'VOCÊ PRECISA INFORMAR SEU E-MAIL.' : 'VOCÊ PRECISA INFORMAR SEU \nE-MAIL.'} 
            closeAction={() =>  this.setModalVisible(false)}/>
            
            <Alert 
            visible={this.props.forgot.alert && this.props.forgot.success}
            icon="envelope"
            mensagem={'ENVIAMOS UM EMAIL\nPARA VOCÊ'}
            descricao={this.props.forgot.data}
            btnLabel="VOLTAR AO LOGIN"
            closeAction={() =>  {
              this.props.closeModal();
              this.props.navigation.navigate("Login") 
              }}/>

            <Alert 
            visible={this.props.forgot.alert && !this.props.forgot.success}
            icon="exclamation"
            mensagem={this.props.forgot.error.Message ? this.props.forgot.error.Message.toUpperCase() : 'Erro desconhecido.Por favor, tente novamente mais tarde.'}
            btnLabel="VOLTAR"
            closeAction={() =>  {
              this.props.closeModal();
              }}/>
                
          </KeyboardAwareScrollView>
          
    );
  }
}


function mapStateToProps(state) {
  return {
    forgot: state.forgot,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ForgotActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(esqueciSenha);
