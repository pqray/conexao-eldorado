import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { StyleSheet, Platform } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: metrics.screenHeight,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  logo: {
    marginTop: Platform.OS == 'ios'? 25 : 0,
    width: 200,
    height: 124,
    resizeMode: 'contain'
  },
  txtEsquecisenha: {
    color: colors.greenmoss,
    marginTop: 20,
    marginBottom: 20,
    ...ifIphoneX({
      marginBottom: -10
    }),
    fontSize: metrics.defaulth3,
    fontWeight: 'bold'
  },
  txtDescricao: {
    marginBottom: Platform.OS == 'ios'? 15 : 0,
    fontSize: Platform.OS == 'ios' ? metrics.fontpIos: metrics.fontp,
    color: colors.gray,
    width: "85%",
    textAlign: "center",
    lineHeight: 19,
    ...ifIphoneX({
      marginBottom: 35
    }),
  },
  camposInput: {
    fontSize: metrics.fontp,
    width: "100%",
    borderBottomWidth: 1,
    color: colors.gray,
    borderBottomColor: colors.gray,
    paddingLeft: 25,
    marginTop: Platform.OS == 'ios'? -15 : -33,
    paddingBottom: Platform.OS == 'ios'? 10 : 0,
  }, 
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: "100%",
    height: 43,
    color: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    ...ifIphoneX({
      marginTop: -30
    })
  },
  buttonContainer: {
    height: Platform.OS == 'ios' ? '27%' :'40%',
    width: '85%',
    flexDirection: "row",
    justifyContent: "center",
    marginTop: Platform.OS == 'ios'? 40 : 0,
    ...ifIphoneX({
      height: 'auto',    
      marginBottom: 250
    })
  },
  txtButton: {
    color: colors.white,
    fontFamily: 'Open Sans',
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: "center",
  },
  iconEnvelope: {
    marginTop: Platform.OS == 'ios'? 0 : 30,
  },  
  footer: {
    marginTop: 0,
    width: '100%',
    height: 60,
    backgroundColor: colors.gray,
    textAlign: 'center'
  },
  txtFooter: {
    color: colors.white,
    fontSize: 14,
    textAlign: 'center',
    lineHeight: 60
  },
  boldTextStyle: {
    fontSize: 14,
    fontWeight: 'bold',
  },  
});
export default styles;