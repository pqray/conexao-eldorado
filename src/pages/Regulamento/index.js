import React from 'react';
import {
    Text,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    View,
    Dimensions,
    Platform
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';

import HTML from 'react-native-render-html';

import { SafeAreaView } from 'react-navigation';
import enums from '../../enums';
import general from '../../config/general';
import Loader from '../../components/loading';
import HeaderTitulo from '../../components/headerTitulo';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';

//redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as PaginasActions } from '../../store/ducks/paginas';


class Regulamento extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: 'Useless Placeholder' };
    }

    async componentDidMount() {
        //let token = await AsyncStorage.getItem("UserData");
        let user = await AsyncStorage.getItem("UserData");
        let token = user.replace('"', '').replace('"', '')
        this.props.getPaginasRequest(token)
    }

    render() {
        const { navigation, paginas } = this.props;
        const Regulamento = "<regulamento>" + paginas.data.Regulamento + "</regulamento>";

        return (


            Platform.OS == 'ios' ?
                <SafeAreaView style={{ backgroundColor: colors.greenmosslight }}>
                    <View style={{ backgroundColor: colors.grayrow }}>
                        <Loader
                            loading={this.props.paginas.loading}
                        />

                        <HeaderTitulo titulo="REGULAMENTO" navigation={this.props.navigation} />

                        <ScrollView contentContainerStyle={{ paddingBottom: 100 }}>
                            <View style={styles.container}>
                                <HTML tagsStyles={{
                                    h1: { color: colors.gray, fontSize: metrics.defaultFontSizeTitle, marginBottom: 10 },
                                    h2: { color: colors.greenmoss, fontSize: metrics.defaultFontSizeTitle },
                                    div: { textAlign: 'left', paddingBottom: 50, paddingTop: 15 }
                                }} html={Regulamento} imagesMaxWidth={Dimensions.get('window').width} />
                            </View>
                        </ScrollView>

                    </View>
                </SafeAreaView>

                :

                <ScrollView>
                    <View style={styles.header}>
                        <Loader
                            loading={this.props.paginas.loading}
                        />
                        <View style={styles.containerHeader}>
                            <TouchableOpacity activeOpacity={0.9} style={styles.btnIconBack} onPress={() => navigation.goBack(null)}>
                                <Icon name="chevron-left" size={20} style={styles.iconBack} />
                            </TouchableOpacity>
                            <Text style={styles.title}>REGULAMENTO</Text>
                        </View>
                    </View>

                    <View style={styles.container}>
                        <HTML tagsStyles={{
                            h1: { color: colors.gray, fontSize: metrics.defaultFontSizeTitle, marginBottom: 10 },
                            h2: { color: colors.greenmoss, fontSize: metrics.defaultFontSizeTitle },
                            div: { textAlign: 'left', paddingBottom: 15, paddingTop: 15 }
                        }} html={Regulamento} imagesMaxWidth={Dimensions.get('window').width} />
                    </View>
                </ScrollView>


        );
    }
}

const mapStateToProps = state => ({
    paginas: state.paginas,
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        ...PaginasActions,
    }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Regulamento);

