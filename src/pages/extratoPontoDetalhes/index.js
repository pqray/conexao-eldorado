import React from 'react';
import {
    View,
    ImageBackground,
    Text,
    Image,
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';

import moment from 'moment';
class extratoPontosDetalhes extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { data } = this.props;
        const Date = data.Data
        const dataFormatada = moment(Date).format("DD/MM/YYYY")
        //console.log('DATA', data)
        //console.log(data.Descricao);
        return (
            
            <View style={styles.header}> 
                <Image
                    source={require('../../assets/bgDetalhesCupom.png')}
                    style={
                        styles.imgTopo
                    }
                />
                <View style={styles.header}>
              
                    <Text style={styles.title}>DETALHE DA OPERAÇÃO</Text>

                {
                    data.Tipo=='AGUARDANDO VALIDAÇÃO'
                    ?<Image
                    source={require('../../assets/ico-extrato-1.png')}
                    style={
                    styles.logo
                    }
                    /> 
                    :null
                }
                  
                  {
                      data.Tipo=='RESGATE DE RECOMPENSA'
                      ?<Image
                      source={require('../../assets/ico-extrato-2.png')}
                      style={
                      styles.logo
                      }
                  />   
                      :null
                  }
                 

                 {
                       data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='BÔNUS POR ACESSO' || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                      ?<Image
                      source={require('../../assets/ico-extrato-4.png')}
                      style={
                      styles.logo
                      }
                  />   
                      :null
                  }

                    {
                      data.Tipo=='NOTA ESTORNADA' || data.Tipo=='RESGATE ESTORNADO'
                      ?<Image
                      source={require('../../assets/ico-extrato-5.png')}
                      style={
                      styles.logo
                      }/>   
                      :null
                    }

{
                      data.Tipo=='NOTA REJEITADA' || data.Tipo=='NOTA INVÁLIDA' || data.Tipo=='PONTOS EXPIRADOS'
                      ?<Image
                      source={require('../../assets/ico-extrato-3.png')}
                      style={
                      styles.logo
                      }/>   
                      :null
                    }
                 {
                      data.Tipo=='NOTA CADASTRADA'
                      ?<Image
                      source={{uri: data.LojaLogo}}
                      style={
                      styles.logo
                      }/>   
                      :null
                    }
                 
                {
                    data.Tipo=='NOTA CADASTRADA'
                    ?<Text style={styles.nameLoja}>{data.LojaNome}</Text>
                    :
                    <Text style={styles.nameLoja}>
                        {
                            data.Tipo == 'BÔNUS POR ANIVERSÁRIO DO USUÁRIO' ? 
                            'BÔNUS DE ANIVERSÁRIO'
                            : 
                            data.Tipo
                        }
                    </Text>
                    
                }
                </View>
                {
                    data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='NOTA REJEITADA' || data.Tipo=='NOTA ESTORNADA' || data.Tipo=='AGUARDANDO VALIDAÇÃO' || data.Tipo=='RECOMPENSA RESGATADA' || data.Tipo=='RESGATE DE RECOMPENSA' || data.Tipo=='RESGATE DE RECOMPENSA' || data.Tipo=="BÔNUS POR ACESSO" || data.Tipo=='NOTA INVÁLIDA' || data.Tipo=='RESGATE ESTORNADO' || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO" || data.Tipo=="PONTOS EXPIRADOS"
                    ?<View>
                        <View style={styles.info}>
                        {
                            data.Tipo=='AGUARDANDO VALIDAÇÃO'
                            ?<View style={styles.info}>
                                <Text style={styles.txtInfo}>Data do Cadastro</Text>
                                <Text style={styles.txtDestaque}>{dataFormatada}</Text>
                            </View>
                            :null
                        }{
                            data.Tipo=='NOTA REJEITADA' || data.Tipo=='NOTA ESTORNADA' || data.Tipo=='RESGATE ESTORNADO' || data.Tipo=='NOTA INVÁLIDA' || data.Tipo=='PONTOS EXPIRADOS'
                            ?<View style={styles.info}>
                                <Text style={styles.txtInfo}>{data.Tipo=='PONTOS EXPIRADOS'?"Pontos adquiridos em data":
                                
                                data.Tipo == 'NOTA ESTORNADA' || data.Tipo=='RESGATE ESTORNADO' ? 'Data do Estorno' :  "Data do Cadastro da Nota"
                                
                                }</Text>
                                <Text style={styles.txtDestaque}>{dataFormatada}</Text>
                            </View>
                            :null
                        }
                        {
                           data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='RECOMPENSA RESGATADA' || data.Tipo=='RESGATE DE RECOMPENSA' || data.Tipo=="BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                            ?<View style={styles.info}>
                                {
                                    data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=="BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                                    ?<Text style={styles.txtInfo}>Data do Recebimento</Text>
                                    :<Text style={styles.txtInfo}>Data do Resgate</Text>
                                }
                                <Text style={styles.txtDestaque}>{dataFormatada}</Text>
                            </View>
                            :null
                        }
                    </View>
                        {
                            data.Tipo=="PONTOS EXPIRADOS"? <View style={styles.infoNone}></View>
                            :
                            data.Tipo=='RECOMPENSA RESGATADA' || data.Tipo=='RESGATE DE RECOMPENSA'
                            ?<View style={styles.info}>
                                <Text style={styles.txtDestaque}>{data.Descricao}</Text>
                            </View>
                            :<View style={styles.info}>
                            {
                                data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=="BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                                ?<View style={{ alignItems: 'center' }}>
                                    <Text style={styles.txtInfo}>Validade dos Pontos</Text>
                                    <Text style={styles.txtDestaque}>{data.DataValidadePonto}</Text>
                                </View>
                                :<View>
                                    {
                                        data.Tipo=="NOTA ESTORNADA" || data.Tipo=='RESGATE ESTORNADO' || data.Tipo=="NOTA INVÁLIDA" || data.Tipo=='AGUARDANDO VALIDAÇÃO'
                                        ?
                                            data.Tipo=='AGUARDANDO VALIDAÇÃO'?
                                            <Text style={styles.txtInfoValidacao}>{"Aguarde, sua solicitação será validada."}</Text>
                                            :<Text style={styles.txtInfo}>{ data.Tipo=="NOTA INVÁLIDA"? "Motivo da não Validação": "Motivo do Estorno"}</Text>

                                        :<Text style={styles.txtInfoMensagem}>{data.Descricao}</Text>
                                    }
                                </View>
                            }    
                                <Text style={styles.txtDestaque}>{data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO" || data.Tipo=='PONTOS EXPIRADOS'? null : data.Descricao}</Text>
                            </View>
                        }
                    </View>
                    :<View>
                        <View style={styles.info}>
                            <Text style={styles.txtInfo}>Data da compra</Text>
                            <Text style={styles.txtDestaque}>{data.DataFormatada}</Text>
                        </View>
                        <View style={styles.info}>
                            <Text style={styles.txtInfo}>Valor da compra</Text>
                            <Text style={styles.txtDestaque}>{data.ValorNota}</Text>
                            { 
                            data.Liberado 
                            ?null
                            :<Text style={styles.txtInfo}>Status:Cupom Válido</Text>
                            }
                        </View>
                        <View style={styles.infoT}>
                            <Text style={styles.txtInfo}>Validade dos Pontos</Text>
                            <Text style={styles.txtDestaque}>{data.DataValidadePonto}</Text>
                        </View>
                    </View>
                }
                
                
                <View style={[styles.footer,
                {
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
                }
                ]}>
                 {
                    
                    data.Tipo=='RESGATE ESTORNADO' || data.Tipo=='NOTA REJEITADA' || data.Tipo=='NOTA INVÁLIDA' || data.Tipo=='NOTA ESTORNADA' || data.Tipo=='PONTOS EXPIRADOS'
                    ?
                    <ImageBackground
                        resizeMode='cover'
                        // source={require('../../assets/bgFotterExtrato.png')}
                        source={require('../../assets/bgFotterExtratoR.png')}
                        // source={require('../../assets/bgFotterExtratoG.png')}
                        style={{ width: '100%', height: 79, borderTopColor: '#ffffff',
                        borderBottomLeftRadius: 4,
                        borderBottomRightRadius: 4, }}
                    >
                   <View style={[styles.footerContainer,
                                {
                                    borderBottomLeftRadius: 4,
                                    borderBottomRightRadius: 4,
                                }
                            ]}>
                            {
                                data.Tipo=='NOTA INVÁLIDA'
                                ?null
                                :<View style={styles.vIcone}>
                               
                                {
                                    data.Tipo=='NOTA ESTORNADA' || data.Tipo=='RESGATE ESTORNADO' || data.Tipo=='PONTOS EXPIRADOS'
                                    ?<Icon name="minus" style={[styles.icon, styles.iconY]} />
                                    :null
                            }                     
                            </View>
                            }
                            {
                            data.Tipo=='NOTA ESTORNADA' || data.Tipo=='NOTA REJEITADA' || data.Tipo=='NOTA INVÁLIDA' || data.Tipo=='RESGATE ESTORNADO' || data.Tipo=='PONTOS EXPIRADOS'
                            ?<View style={[styles.pontos, {textAlign: 'center', flexDirection: 'column',  alignItems: 'center'}]}>
                                <Text style={styles.pontosDestaque}>{data.Tipo=='PONTOS EXPIRADOS' ?"PONTOS EXPIRADOS":"SEM PONTOS"}</Text>
                                <Text style={styles.pontosDestaque}>{data.Tipo=='PONTOS EXPIRADOS' ?`${data.Pontos} PONTOS`:"GERADOS"}</Text>
                            </View>
                            :<View style={[styles.pontos, {textAlign: 'center', width: '100%', flexDirection: 'column', alignItems: 'center'}]}>
                                <Text style={styles.pontosInfo}>PONTOS [GERADOS/DEBITADOS]</Text>
                                <Text style={styles.pontosDestaque}>{data.Pontos} PONTOS</Text>
                            </View>
                            }
                            
                        </View>
                        </ImageBackground>
                        :null
                    }
                    {
                    data.Tipo=='RESGATE DE RECOMPENSA'
                    ?
                    <ImageBackground
                        resizeMode='cover'
                        // source={require('../../assets/bgFotterExtrato.png')}
                        // source={require('../../assets/bgFotterExtratoR.png')}
                        source={require('../../assets/bgFotterExtratoG.png')}
                        style={{ width: '100%', height: 79, borderTopColor: '#ffffff' }}
                    >
                <View style={[styles.footerContainer,{
                    }]}>
                        <View style={styles.vIcone}>
                            {/*Icone Verde*/}
                            {
                                <Icon name="minus" style={[styles.icon, styles.iconG]} />
                            }
                        </View>

                        <View style={[styles.pontos]}>
                            <Text style={styles.pontosInfo}>PONTOS DEBITADOS</Text>
                            <Text style={styles.pontosDestaque}>{data.Pontos} PONTOS</Text>
                        </View>
                            
                    </View>
                    </ImageBackground>
                        :null
                    }
                    {
                        data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='AGUARDANDO VALIDAÇÃO' || data.Tipo=='NOTA CADASTRADA' || data.Tipo=='BÔNUS POR ACESSO' || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                    ?
                    <ImageBackground
                        resizeMode='cover'
                        source={require('../../assets/bgFotterExtrato.png')}
                        // source={require('../../assets/bgFotterExtratoR.png')}
                        // source={require('../../assets/bgFotterExtratoG.png')}
                        style={{ width: '100%', height: 79, borderTopColor: '#ffffff' }}
                    >
                    <View style={[styles.footerContainer,
                                {
                                    // backgroundColor: '#e4b443',
                                }
                            ]}>
                            {
                            data.Tipo=='AGUARDANDO VALIDAÇÃO'
                            ?null
                            :<View style={styles.vIcone}>
                                {/*Icone Verde*/}
                                {
                                    data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='NOTA CADASTRADA' || data.Tipo=='BÔNUS POR ACESSO' || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                                    ?<Icon name="plus" style={[styles.icon, styles.iconG2]} />
                                    :null
                                }
                                {/*Icone Vermelho*/}
                                {
                                    data.TipoDescricao=='Resgate' && data.TipoDescricao=='Nota estornada' && data.TipoDescricao=='Nota Inválida'
                                    ?<Icon name="plus" style={[styles.icon, styles.iconR]} />
                                    :null
                                }
                
                            </View>
                            }
                            {
                            data.Tipo=='AGUARDANDO VALIDAÇÃO'
                            ?<View style={[styles.pontos]}>
                                <Text style={styles.pontosDestaque}>SEM PONTOS</Text>
                                <Text style={styles.pontosDestaque}>GERADOS</Text>
                            </View>
                            :null
                            }
                            {
                                data.Tipo == "BÔNUS POR ANIVERSÁRIO DO USUÁRIO" || data.Tipo=='NOTA CADASTRADA' || data.Tipo=='BÔNUS POR ACESSO' || data.Tipo=="**BÔNUS POR ACESSO" || data.Tipo=="**BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="**PONTO EXTRA" || data.Tipo=="BÔNUS POR PRIMEIRO ACESSO" || data.Tipo=="CADASTRO COMPLETO"
                                ?<View style={[styles.pontos]}>
                                    <Text style={styles.pontosInfo}>PONTOS GERADOS</Text>
                                    <Text style={styles.pontosDestaque}>{data.Pontos} PONTOS</Text>
                                </View>
                                :null
                            }
                        </View>
                        </ImageBackground>
                        :null
                    }
                    
                    
                </View>
            </View>
        );
    }
}


export default extratoPontosDetalhes;
