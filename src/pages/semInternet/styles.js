import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';

const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  container: {
    width: "75%",
    height: 385,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  atualizar: {
    width: 140,
    height: 112,
    marginBottom: 100
  },
  h1yellow: {
    color: colors.yellowp,
    marginTop: 15,
    fontSize: metrics.defaulth1,
    width: '70%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  txtDescricao: {
    fontSize: metrics.fontp,
    color: colors.white,
    width: "75%",
    textAlign: "center",
    marginTop: 13,
    marginBottom: 21

  },
  buttonEntrar: {
    borderRadius: 100,
    borderWidth: 1,
    borderColor: colors.yellow,
    width: "100%",
    height: metrics.heightButton,
    width: metrics.widthButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40
  },
  buttonContainer: {
    height: '40%',
    width: '75%',
    flexDirection: "row",
    justifyContent: "center"
  },
  txtButton: {
    color: colors.yellow,
    fontSize: metrics.fontButton,
    fontWeight: 'bold'
  },
  iconMail: {
    fontSize: 32,
    color: '#2F2F2F',
    marginTop: 10,
    marginBottom: 10
  }
});
export default styles;