import {
  StyleSheet
} from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 50,
    backgroundColor: colors.white,
  },
  logo: {
    width: 200,
    height: 114,
    resizeMode: 'contain'
  },
  buttonRedeS: {
    borderRadius: 100,
    backgroundColor: '#000',
    width: "100%",
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 18,
    marginBottom: 18
  },
  logofb: {
    width: 25,
    height: 25
  },
  txtEsquecisenha: {
    color: '#2F2F2F',
    marginTop: 20,
    marginBottom: 20,
    fontSize: metrics.defaulth3,
    fontWeight: "bold",
    fontFamily: 'Zocial',
  },
  txtDescricao: {
    fontSize: metrics.fontp,
    color: "#2F2F2F",
    width: metrics.defaultWidthPag,
    textAlign: "center",
    lineHeight: 17,
    fontFamily: 'Open Sans',
  },
  camposInput: {
    width: "100%",
    borderBottomWidth: 1,
    color: '#2F2F2F',
    borderBottomColor: '#2F2F2F',
    paddingLeft: 25,
    marginTop: -32,
    fontSize: metrics.fontp,
    height: 50
  },
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: '#cf9112',
    width: '80%',
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  buttonEntrarFB: {
    borderRadius: 100,
    backgroundColor: '#fff',
    borderColor: '#c6a41c',
    borderWidth: 3,
    width: '100%',
    height: metrics.heightButton , 
    color: '#c6a41c',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  buttonContainer: {
    width: metrics.defaultWidthPag,
    flexDirection: "row",
    justifyContent: "center"
  },
  txtButton: {
    color: '#fff',
    fontFamily: 'Open Sans',
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: "center",
  },
  txtAceitarTemos: {
    color: '#2F2F2F',
    fontSize: metrics.defaulth4,
    fontWeight: 'bold',
    marginTop: 20
  },
  txtCheckBox: {
    color: '#2F2F2F',
    fontSize: 13,
    marginTop: 8
  },
  checkContainer: {
    flexDirection: "row",
    justifyContent: "center"
  },
  boldTextStyle: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  buttonAgileze: {
    borderRadius: 100,
    backgroundColor: '#0f5084',
    width: "100%",
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 20
  },
  logoSociais: {
    marginLeft: 10
  },
  footer: {
    width: '100%',
    height: 60,
    backgroundColor: colors.gray,
    textAlign: 'center'
  },
  txtFooter: {
    color: '#ffffff',
    fontSize: 14,
    textAlign: 'center',
    lineHeight: 60
  },


  imgBackground: {
    width: '100%',
    height: '100%',
  },
  modalContent: {
    position: 'relative',
  },
  modalContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonVoltar: {
    position: 'absolute',
    top: 15,
    right: 15,
    zIndex: 99
  },
  txtButtonVoltar: {
    color: colors.yellow,
    fontWeight: 'bold',
    fontSize: 24
  },
  alertCont: {
    backgroundColor: '#ffffff',
    width: metrics.defaultWidthModal,
    alignItems: 'center',
    paddingBottom: 30,
    borderRadius: 5
  },
  txtAlertCont: {
    fontWeight: 'bold',
    flexWrap: 'wrap',
    width: '85%',
    textAlign: 'center',
    paddingTop: 30,
    paddingBottom: 10,
    fontSize: metrics.defaultFontSizeTitle
  },
  iconAlert: {
    fontSize: 45,
    marginTop: 30,
    color: '#000000'
  },
  contentModal: {
    height: metrics.screenHeight,
    width: metrics.screenWidth,
    position: 'relative',
    backgroundColor: '#ffffff'
  },
  iconCloseModal: {
    width: '100%',
    position: 'relative',
    textAlign: 'right',
    position: 'absolute',
    top: 10,
    right: 20,
    zIndex: 9
  },
  iconBack: {
    width: '100%',
    position: 'absolute',
    bottom: 40,
    textAlign: 'left',
    marginLeft: '4%',
  },
  iconClose: {
    color: colors.yellow,
  },
  sublinhado: {
    textDecorationLine: 'underline'
  },
  buttonEsqueciSenha: {
    width: "80%",
    height: metrics.heightButton,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  txtEsqueciSenha: {
    color: colors.greenmoss,
    fontSize: metrics.fontButton,
    marginTop: 15,
    fontWeight: 'bold'
  }

});

export default styles;