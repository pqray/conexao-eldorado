
import React from 'react';
import {
  AppRegistry,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Dimensions
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Accordion from 'react-native-collapsible/Accordion';
import HTML from 'react-native-render-html';
import { SafeAreaView } from 'react-navigation';
import colors from '../../styles/colors';
import HeaderTitulo from '../../components/headerTitulo';
//redux
import { connect } from 'react-redux';
import Loader from '../../components/loading';
import { bindActionCreators } from 'redux';
import { Creators as PaginasActions } from '../../store/ducks/paginas';

import { View } from 'native-base';
import metrics from '../../styles/metrics';


class Inicial extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder', activeSections: [] };
  }
  // _renderSectionTitle = section => {
  //   //console.log(section.content);

  //   return (
  //     <View style={styles.content}>
  //       <Text>{section.content}</Text>
  //     </View>
  //   );
  // };

  _renderHeader = (section, i) => {
    return (
      <View>
        <View style={i != 0 ? styles.titacordBorder : styles.titacord}>
          <Text style={styles.textacord}>{section.title}</Text>
          <Icon name="angle-down" style={styles.icoSeta} />
        </View>
      </View>
    );
  };

  _renderContent = section => {
    // //console.log(section.content);
    return (
      <View style={styles.content}>
        <HTML tagsStyles={{ sobre: styles.txtConteudo }} html={section.content} imagesMaxWidth={Dimensions.get('window').width} />

        {/* <Text>{section.content}</Text> */}
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  async componentDidMount() {
    let token = await AsyncStorage.getItem("UserData");
    this.props.getPaginasRequest(token)
  }

  render() {
    const { navigation, paginas } = this.props;
    const Sobre = "<sobre>" + paginas.data.SobreOPrograma + "</sobre>";
    const Sobre_Topo = "<sobre>" + paginas.data.SobreOPrograma_topo + "</sobre>";
    const Recompensa = "<sobre>" + paginas.data.Recompensa + "</sobre>";
    const Beneficio = "<sobre>" + paginas.data.Beneficios + "</sobre>";



    // //console.log(paginas.data.SobreOPrograma);
    // //console.log(Regulamento)


    var SECTIONS = [];

    if (paginas.data.Recompensa)
      SECTIONS.push({
        title: paginas.data.Recompensa.Titulo,
        content: paginas.data.Recompensa.Texto
      }
      )

    if (paginas.data.Beneficios)
      SECTIONS.push({
        title: paginas.data.Beneficios.Titulo,
        content: paginas.data.Beneficios.Texto
      })


    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.greenmosslight, marginBottom: 1 }}>
        <View style={{ flex: 1, height: metrics.screenHeight, backgroundColor: colors.grayrow }}>
        <Loader
              loading={this.props.paginas.loading}
            />
            
            <HeaderTitulo titulo="CONEXÃO ELDORADO" navigation={this.props.navigation} />

          <ScrollView>

            <View style={styles.container}>
              <Image
                source={require('../../assets/logo.png')}
                style={
                  styles.logo
                }
              />
              {paginas.data.SobreOPrograma_topo ?
                <HTML tagsStyles={{ sobre: styles.txtChamada }} html={Sobre_Topo} imagesMaxWidth={Dimensions.get('window').width} />
                : null}

              <View style={styles.buttonContainer}>
                <TouchableOpacity activeOpacity={0.8} style={styles.buttonRegulamento} onPress={() => { navigation.navigate('regulamento') }}>
                  <Text style={styles.txtButton}> REGULAMENTO </Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.buttonDuvidas} onPress={() => { navigation.navigate('DuvidasFrequentesStack') }}>
                  <Text style={styles.txtButton}> DÚVIDAS </Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.titleConteudo}>CONEXÃO ELDORADO</Text>
              <View style={styles.containerTxt}>

                {paginas.data.SobreOPrograma ?
                  <HTML tagsStyles={{ sobre: styles.txtConteudo }} html={Sobre} imagesMaxWidth={Dimensions.get('window').width} />
                  : null}

                {
                  SECTIONS.length > 0 ?
                    <Accordion
                      sections={SECTIONS}
                      activeSections={this.state.activeSections}
                      renderSectionTitle={this._renderSectionTitle}
                      renderHeader={this._renderHeader}
                      renderContent={this._renderContent}
                      onChange={this._updateSections}
                    />
                    : null
                }

              </View>



            </View>

          </ScrollView>

        </View>
      </SafeAreaView>

    );

  }

  // render() {
  //     const { navigation, paginas } = this.props;
  //     const Sobre = "<sobre>" + paginas.data.SobreOPrograma + "</sobre>";
  //     const Sobre_Topo = "<sobre>" + paginas.data.SobreOPrograma_topo + "</sobre>";

  //     return (
  //         <ScrollView>
  //             <View style={styles.header}>
  //                 <View style={styles.container}>
  //                     <TouchableOpacity activeOpacity={0.9} style={styles.btnIconBack} onPress={() => navigation.goBack(null)}>
  //                         <Icon name="chevron-left" size={20} style={styles.iconBack} />
  //                     </TouchableOpacity>
  //                     <Text style={styles.hTitle}>PONTEPLUS</Text>
  //                 </View>
  //             </View>


  //             <View style={styles.container}>
  //                 <Image
  //                     source={require('../../assets/logobranca.png')}
  //                     style={
  //                         styles.logo
  //                     }
  //                 />
  //                 <HTML tagsStyles={{ sobre: styles.txtChamada }} html={Sobre_Topo} imagesMaxWidth={Dimensions.get('window').width} />

  //                 <View style={styles.buttonContainer}>
  //                     <TouchableOpacity activeOpacity={0.8} style={styles.buttonRegulamento} onPress={() => { navigation.navigate('regulamento') }}>
  //                         <Text style={styles.txtButton}> REGULAMENTO </Text>
  //                     </TouchableOpacity>
  //                     <TouchableOpacity activeOpacity={0.8} style={styles.buttonDuvidas} onPress={() => { navigation.navigate('DuvidasFrequentesStack') }}>
  //                         <Text style={styles.txtButton}> DÚVIDAS </Text>
  //                     </TouchableOpacity>
  //                 </View>
  //                 <Text style={styles.titleConteudo}> PONTEPLUS </Text>
  //                 <View style={styles.containerTxt}>
  //                     <HTML tagsStyles={{ sobre: styles.txtConteudo }} html={Sobre} imagesMaxWidth={Dimensions.get('window').width} />
  //                 </View>



  //             </View>


  //         </ScrollView>
  //     );
  // }
}

const mapStateToProps = state => ({
  paginas: state.paginas,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...PaginasActions,
  }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Inicial);
