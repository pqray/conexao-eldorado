import {
    StyleSheet,Platform
} from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { ifIphoneX } from 'react-native-iphone-x-helper';


const styles = StyleSheet.create({
    imgItem: {
        width: '100%',
        height: 250,
        resizeMode: 'cover'
    },
    transitionWidth: {
        position: 'relative',
    },
    containerDetalhes: {
        flexWrap: 'wrap',
        backgroundColor: '#fff',
        marginTop: -10,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 10,
        padding: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        position: 'relative',
        marginBottom: 10
    },
    containerTitle:{
        flexDirection: 'column',
        justifyContent: 'flex-start',
        marginBottom: 10,
    },
    titulo: {
        fontSize: metrics.defaultFontSizeTitle,
        color: colors.gray,
        fontWeight: 'bold',
        width: '100%',
        height: 'auto',
    },
    containerPontos: {
        flexDirection: 'row',
    },
    pontos: {
        fontSize: 25,
        color: colors.greenmoss,
        fontWeight: 'bold',
    },
    txtPontos: {
        fontSize: 12,
        fontWeight: 'bold',
        marginTop: 4,
        marginLeft: 3,
    },
    descricao: {
        fontSize: metrics.defaultFontSizeTxt,
        lineHeight: metrics.defaultLineHeightTxt,
        color: colors.gray,
        width: '100%',
    },
    prazo: {
        marginTop: 30,
        justifyContent: 'center',
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'row'
    },
    iconePrazo: {
        marginRight: 10,
        color: '#000',
        fontSize: metrics.defaultFontSizeTitle,
        lineHeight: metrics.defaultLineHeightTxt,
        fontWeight: 'bold'
    },
    txtPrazo: {
        color: '#000',
        fontSize: 12,
        lineHeight: metrics.defaultLineHeightTxt,
    },
    btnUnidades: {
        height: metrics.heightButton,
        justifyContent: 'center',
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderColor: colors.btResgate,
        marginTop: 10
    },
    txtUnidades: {
        fontSize: metrics.fontButton,
        color: colors.btResgate,
        fontWeight: 'bold',
        width: '100%',
        lineHeight: 39,
        textAlign: 'center'
    },
    btnResgate: {
        height: metrics.heightButton,
        justifyContent: 'center',
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: colors.btResgate,
        borderRadius: 20,
        marginTop: 5,
        marginBottom: 50
    },
    btnAviseme: {
        height: metrics.heightButton,
        justifyContent: 'center',
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: colors.yellow,
        borderRadius: 20,
        marginTop: 15,
        marginBottom: 50
    },
    btnRecusar: {
        backgroundColor: colors.graylight,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginRight: 5
    },
    btnAceitar: {
        backgroundColor: colors.yellow,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginLeft: 5
    },
    btnIndisponivel: {
        height: metrics.heightButton,
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: '#999',
        borderRadius: 20,
        marginTop: 15,
        marginBottom: 50
    },
    txtResgate: {
        fontSize: metrics.fontButton,
        fontWeight: 'bold',
        color: '#ffffff',
        textAlign: 'center',
        width: '100%',
        lineHeight: metrics.heightButton,
    },
    txtIndisponivel: {
        color: colors.yellow,
        width: '100%',
        textAlign: 'center',
        fontSize: metrics.fontButton,
        marginTop: 15,
        fontWeight: 'bold',
        marginBottom: -5
    },
    containerModal: {
        backgroundColor: '#ffffff',
        width: metrics.defaultWidthModal,
        alignItems: 'center',
        paddingBottom: 30,
        paddingTop: 20,
        borderRadius: 5
    },
    iconAlert: {
        fontSize: 45,
        marginTop: 30,
        color: '#000000'
    },
    textModalAvise: {
        fontSize: 14,
        color: "#2F2F2F",
        width: "85%",
        textAlign: "center",
        lineHeight: 17
    },
    titleModalAviseTitulo: {
        fontWeight: 'bold',
        flexWrap: 'wrap',
        width: '85%',
        textAlign: 'center',
        paddingTop: 30,
        paddingBottom: 10,
        fontSize: metrics.defaultFontSizeTitle
    },
    titleModalAvise: {
        fontSize: metrics.defaultFontSizeTxt,
        flexWrap: 'nowrap',
        color: "#2F2F2F",
        width: "85%",
        textAlign: "center",
        lineHeight: 17
    },
    bold: {
        fontWeight: 'bold'
    },
    containerBtns: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 12
    },
    btnNo: {
        backgroundColor: colors.yellow,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginRight: 5
    },
    btnYes: {
        backgroundColor: colors.btnYes,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginLeft: 5
    },
    txtBtns: {
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#ffffff',
        lineHeight: 35,
        flexWrap: 'wrap'
    },
    txtBtnsConfirm: {
        color: '#fff',
        fontFamily: 'Open Sans',
        fontSize: 15,
        fontWeight: 'bold',
        justifyContent: "center",
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer2: {
        width: metrics.defaultWidthPag,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 10,
        width: '100%',
    },
    btnFechar: {
        borderRadius: 100,
        backgroundColor: colors.yellow,
        width: '85%',
        height: metrics.heightButton,
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    iconSuccess: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 10
    },
    iconTimes: {
        width: '100%',
        textAlign: 'right',
        position: 'absolute',
        top: 10,
        color: colors.yellow,
        fontWeight: 'bold',
        paddingRight: 20
    },
    allModal: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerFull: {
        flex: 1,
        paddingBottom: 0,
        ...ifIphoneX({
            paddingBottom: 0
        }),
        backgroundColor: colors.bgContainerFull
    }
});

export default styles;