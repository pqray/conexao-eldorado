
import React from 'react';
import { TouchableOpacity, Text, View, Image, ImageBackground, BackHandler, ToastAndroid } from 'react-native';

import styles from './styles';
import AppLink from 'react-native-app-link';

class verificaVersao extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder' };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    ToastAndroid.show('Você precisa atualizar a versão de seu aplicativo', ToastAndroid.SHORT);
    return true;
  }

  openPage() {
    AppLink.openInStore({ appName: "Conexão Eldorado", appStoreId: "1204922579", appStoreLocale: "br", playStoreId: "com.am4.conexaoeldorado" }).then(() => {
      // do stuff
    })
  }

  render() {
    return (
      <ImageBackground style={styles.imgBackground}
        resizeMode='cover'
        source={require('../../assets/bgsem.png')}>


        <View style={styles.container}>
          <Image
            source={require('../../assets/atualizar.png')}
            style={
              styles.atualizar
            }
          />


          <Text style={styles.h1yellow}>NOVA ATUALIZAÇÃO</Text>

          <Text style={styles.txtDescricao}>É necessário que atualize o aplicativo antes de prosseguir.</Text>



          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttonEntrar} onPress={() => this.openPage()}>
              <Text style={styles.txtButton}> ATUALIZAR O APLICATIVO </Text>
            </TouchableOpacity>
          </View>

        </View>

      </ImageBackground>
    );
  }
}



export default verificaVersao;
