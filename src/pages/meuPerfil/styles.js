import {
    StyleSheet,
    Platform
} from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import {
    ifIphoneX
} from 'react-native-iphone-x-helper'
const styles = StyleSheet.create({
    headerUsuario: {
        width: '100%'
    },
    logo: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 80,
        width: 186,
        height: 41,
    },
    containerImgUsuario: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: -55,
        height: 120,
        width: 120,
        position: 'relative',
    },

    imgUsuario: {
        position: 'absolute',
        height: 120,
        width: 120,
        borderRadius: 60,
        marginTop: 5,
        borderWidth: 6,
        borderColor: colors.greenmoss
    },
    bold: {
        fontWeight: 'bold'
    },
    bgBranco: {
        backgroundColor: '#ffffff',
    },
    btnImagem: {
        height: 30,
        width: 30,
        position: 'absolute',
        backgroundColor: colors.yellow,
        borderRadius: 15,
        textAlign: 'center',
        right: 10,
        bottom: 0
    },
    icoImagem: {
        fontSize: 17,
        color: colors.white,
        lineHeight: 30,
        textAlign: 'center'
    },
    alertDados: {
        backgroundColor: colors.greenmoss,
        width: 280,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 40,
        borderRadius: 4,
        position: 'relative'
    },
    arrowTop: {
        position: 'absolute',
        top: -14,
        right: 118
    },
    titleDados: {
        color: '#ffffff',
        textAlign: 'center',
        paddingTop: 20,
        fontWeight: 'bold',
        fontSize: 16
    },
    desdDados: {
        color: '#ffffff',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 20,
        fontSize: 12,
        paddingLeft: 40,
        paddingRight: 40
    },
    icon: {
        fontSize: 14
    },
    iconCel: {
        fontSize: 25,
        marginTop: -3
    },
    titleItem: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    valItem: {
        fontSize: 14,
        flex: 1,
        paddingRight: 15,
    },
    item: {
        flexDirection: 'row',
        marginTop: 20,
    },
    vitem: {
        width: '100%'
    },
    vIcon: {
        width: 40,
        paddingLeft: 15,
        paddingTop: 3
    },
    buttonAlterar: {
        borderRadius: 100,
        backgroundColor: colors.bgButton,
        width: metrics.defaultWidthPag,
        height: 35,
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    buttonContainer: {
        width: '100%',
        justifyContent: "center",
        alignItems: 'center'
    },
    txtButton: {
        color: colors.white,
        fontSize: metrics.defaultFontSizeTxt,
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: 35
    },
    txtButtonFb: {
        color: colors.gray,
        fontSize: metrics.defaultFontSizeTxt,
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: 35
    },
    buttonDados: {
        borderRadius: 100,
        backgroundColor: colors.gray,
        width: metrics.defaultWidthPag,
        height: 35,
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtCancel: {
        fontSize: metrics.defaultFontSizeTxt,
        textAlign: 'center',
        marginBottom: 20,
        width: 280,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    txtBack: {
        position: 'absolute',
        width: metrics.defaultWidthPag,
        paddingTop: 30,
        marginLeft: 20,
        zIndex: 1
    },
    iconBack: {
        color: '#ffffff',
    },
    buttonRedeS: {
        borderRadius: 100,
        backgroundColor: colors.grayrow,
        width: metrics.defaultWidthPag,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 30,
        flexDirection: 'row',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerModal: {
        width: metrics.defaultWidthModal,
        backgroundColor: colors.white,
        padding: 20,
        borderRadius: 8
    },
    btnFechar: {
        height: 35,
        width: '100%',
        backgroundColor: colors.btnYes,
        borderRadius: 50,
        marginTop: 15
    },
    allModal: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtBtns: {
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold',
        color: colors.white,
        lineHeight: 35
    },
    titleModalAvise: {
        fontSize: metrics.defaulth2,
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    btnNo: {
        backgroundColor: colors.yellow,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginRight: 5
    },
    btnYes: {
        backgroundColor: colors.btnYes,
        borderRadius: 100,
        width: '40%',
        height: metrics.heightButton,
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginLeft: 5
    },
    iconTimes: {
        width: '100%',
        textAlign: 'right',
        position: 'absolute',
        top: 10,
        color: colors.yellow,
        fontWeight: 'bold',
        paddingRight: 20
    },
    containerBtns: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textModalAvise: {
        marginTop: 15,
        marginBottom: 10
    },
    vItem: {
        flex: 1,
        flexDirection: 'column',
    },
    itens: {
        width: '100%',
        marginTop: 15,
        marginBottom: 25,
    },
    logofb: {
        marginLeft: 5,
        width: 10,
        height: 21
    },
});

export default styles;