import React from 'react';
import { Text, View, Image, ScrollView,Dimensions, AsyncStorage } from 'react-native';
import styles from './styles';
import metrics from '../../styles/metrics';
import colors from '../../styles/colors';
import Loading from '../../components/loading';

import Icon from 'react-native-vector-icons/FontAwesome';

import HTML from 'react-native-render-html';


//redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as PaginasActions } from '../../store/ducks/paginas';
import { ifIphoneX } from 'react-native-iphone-x-helper';

class termosUso extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder' };
  }
  
  async componentDidMount() {
    this.props.getPaginasRequest()
  }

  render() {
    const { paginas,navigation } = this.props;
    const { goBack } = navigation;

    const Termos = "<regulamento>" + paginas.data.TermosUso.Texto + "</regulamento>";

    return (
      <ScrollView>
        <Loading loading={this.props.paginas.loading} />

        <View style={styles.container}>
          <Text style={styles.iconTimes} onPress={() => goBack()}>
            <Icon name="times" size={25}/>
          </Text>

          <View style={styles.conteudo}>
            <Image
              source={require('../../assets/logo.png')}
              style={
                styles.logo
              }
            />

            <Text style={styles.txtEsquecisenha}>TERMO DE USO E POLÍTICA DE PRIVACIDADE</Text>
            
            <HTML tagsStyles={{ 
              h1: { color: '#000', fontSize: metrics.defaultFontSizeTitle, marginBottom: 10 },
              h2: { color: colors.yellow, fontSize: metrics.defaultFontSizeTitle },
              div: { textAlign: 'left', paddingBottom: 15, paddingTop: 15 }
            }} html={Termos} imagesMaxWidth={Dimensions.get('window').width} />

  

          </View>

          <View style={styles.footer}>
            <Text style={styles.iconBack} onPress={() => goBack()} >
              <Icon name="chevron-left" size={20} />
            </Text>
          </View>
        </View>
      </ScrollView>

    );
  }
}
const mapStateToProps = state => ({
  paginas: state.paginas,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      ...PaginasActions,
  }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(termosUso);
