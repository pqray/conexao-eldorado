
import React from 'react';
import {
    AppRegistry,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import styles from './styles';
import { View } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-navigation';

import HeaderTitulo from '../../components/headerTitulo';
import colors from '../../styles/colors';
class DuvidasFrequentesInterna extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: 'Useless Placeholder' };
    }
    render() {
        const { navigation } = this.props;
        const { goBack } = navigation;
        const { duvidafrequente } = navigation.state.params;

        return (
            <SafeAreaView style={{ backgroundColor: colors.greenmosslight }} forceInset={{ bottom: 'never' }}>
                <View style={styles.containerFull}>
                    <HeaderTitulo titulo="DÚVIDAS FREQUENTES" navigation={this.props.navigation} />

                    <ScrollView style={{ height: '100%' }}>
                        <View style={styles.container}>

                            <Text style={styles.txtTitle}>{duvidafrequente.Pergunta}</Text>

                            <Text style={styles.txtDescripion}>{duvidafrequente.Resposta}</Text>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

export default DuvidasFrequentesInterna;
