import { StyleSheet, Platform } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { ifIphoneX } from 'react-native-iphone-x-helper';

const styles = StyleSheet.create({
    bgPag: {
        backgroundColor: '#F3F3F3'
    },
    header: {
        width: '100%',
        backgroundColor: colors.greenmosslight,
        textAlign: 'center',
    },
    containerHeader: {
        width: metrics.defaultWidthPag,
        paddingTop: Platform.OS == 'ios' ? 15 : 0,
        height: Platform.OS == "ios" ? 70 : 45,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    btnIconBack: {
        height: 45,
        width: '5%',
        lineHeight: 45
    },
    iconBack: {
        color: '#ffffff',
        fontSize: 21,
        lineHeight: 45
    },
    title: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: metrics.defaultLineHeightTxt,
        textAlign: 'center',
        width: '90%',
    },
    container: {
        width: metrics.defaultWidthPag,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    containerPick: {
        width: '100%',
        borderBottomWidth: 1,
        borderColor: colors.graylight
    },
    assunto: {
        color: '#000000',
        fontSize: metrics.defaultFontSizeTxt,
        width: '100%',
        paddingBottom: 10,
        paddingTop: 15
    },
    data: {
        width: '100%',
        fontSize: metrics.defaultFontSizeTxt,
        color: '#B9B9B9'
    },
    bolder: {
        fontWeight: 'bold'
    },
    question: {
        paddingBottom: 15,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 15,
        marginBottom: 20,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        position: 'relative'
    },
    titleQuestion: {
        color: '#000000',
        fontSize: metrics.defaultFontSizeTxt,
    },
    imgQuestion: {
        width: '100%',
        marginTop: 15,
    },
    answer: {
        paddingBottom: 15,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 15,
        marginBottom: 20,
        borderRadius: 5,
        backgroundColor: '#000000',
        position: 'relative'
    },
    arrowBlack: {
        position: 'absolute',
        top: 13,
        left: -13
    },
    arrowWhite: {
        position: 'absolute',
        top: 13,
        right: -13
    },
    titleAnswer: {
        color: '#ffffff',
        fontSize: metrics.defaultFontSizeTxt,
    },
    nameAnswer: {
        fontSize: metrics.defaultFontSizeTxt,
        color: colors.yellow,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    buttonContainer: {
        width: '100%',
        backgroundColor: colors.yellow,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        marginTop: 20,
        marginBottom: 10,
    },
    txtButton: {
        fontSize: metrics.defaultFontSizeTitle,
        color: '#ffffff',
        fontWeight: 'bold',
    },
    /*MODAL*/
    modalTopoTransparent: {
        backgroundColor: 'rgba(6, 6, 6, 0.75)',
        width: '100%',
        position: 'relative',
        height: metrics.screenHeight
    },
    textArea: {
        backgroundColor: '#ffffff',
        height: Platform.OS == 'ios' ? 80 : 80,
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        textAlignVertical: 'top'
    },
    txtForm: {
        fontSize: metrics.defaultFontSizeTxt,
        color: '#000000',
        paddingTop: 15
    },
    inputFile: {
        backgroundColor: '#ffffff',
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        height: 35
    },
    vFile: {
        width: '100%',
        marginTop: 30,
        position: 'relative'
    },
    buttonEnviar: {
        width: '100%',
        backgroundColor: colors.yellow,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        marginTop: 10,
    },
    buttonVoltar: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,

    },
    imgItem: {
        width: '100%',
        height: 250,
        resizeMode: 'cover'
    },
    txtButtonVoltar: {
        color: '#B9B9B9'
    },
    buttonRemoveFile: {
        top: 7,
        right: 100,
        position: 'absolute',
    },
    buttonRemoveFile_text: {
        color: colors.yellow,
    },
    buttonFile: {
        backgroundColor: colors.yellow,
        width: 84,
        height: 27,
        top: 4,
        right: 7,
        position: 'absolute',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtBtnFile: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 13
    },
    camposSelectIOS: {
        width: "100%",
        color: '#2F2F2F',
        paddingLeft: 15,
        paddingTop: 12,
        marginTop: 20,
        marginBottom: 15,
        height: 40,
        width: '100%',
        backgroundColor: '#ffffff',
    },
    camposSelect: {
        width: "100%",
        borderBottomWidth: 1,
        color: '#2F2F2F',
        borderBottomColor: '#2F2F2F',
        paddingLeft: 25,
        marginTop: 20,
        marginBottom: 15,
        height: 40,
        width: '100%',
        backgroundColor: '#ffffff',
    },
});

export default styles;
