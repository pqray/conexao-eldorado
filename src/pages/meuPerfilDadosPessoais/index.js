import React from 'react';
import {
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Platform,
    Text,
    Image,
    TextInput,
    Picker,
    ActionSheetIOS,
    Modal,
    ImageBackground,
    AsyncStorage,
} from 'react-native';
import styles from './styles';
import stylesEndereco from './stylesEndereco';
import { TextInputMask } from 'react-native-masked-text'
import { SafeAreaView } from 'react-navigation';
import { ifIphoneX } from 'react-native-iphone-x-helper'

import Icon from 'react-native-vector-icons/FontAwesome';

import Alerts from '../../components/Alert';
import Loading from '../../components/loading';
import HeaderTitulo from '../../components/headerTitulo';
//redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as AssociadosActions } from '../../store/ducks/associado';
import { Creators as AtualizarActions } from '../../store/ducks/atualizar';
import general from '../../config/general';
import ImagePicker from 'react-native-image-picker';
import { NavigationActions, StackActions } from 'react-navigation';
import axios from 'axios';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const resetAction = StackActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({ routeName: 'meuPerfil' })],
});

var SEXO_BTNS = ['Masculino', 'Feminino', 'VOLTAR'];

const options = {
    title: 'Escolha a sua foto de perfil',
    takePhotoButtonTitle: 'Tirar Foto',
    cancelButtonTitle: 'Cancelar',
    chooseFromLibraryButtonTitle: 'Escolher da Galeria',
    quality: 0.6,
    width: 1024,
    height: 1024,
    fixOrientation: true,
    storageOptions: {
        skipBackup: true,
        path: 'images',
        waitUntilSaved: true
    },
};

class meuPerfilDadosPessoais extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            text: 'Useless Placeholder',
            modalVisibleAlertConcluido: false,
            modalVisibleAlertCancel: false,
            photo: null,
            Nome: '',
            Sobrenome: '',
            DataNascimento: '',
            RG: '',
            Sexo: '1',
            Telefone: '',
            Celular: '',
            TelefoneComercial: '',
            EmailAdicional: '',
            EnderecoLogradouro: '',
            EnderecoNumero: '',
            EnderecoComplemento: '',
            EnderecoBairro: '',
            EnderecoCidade: '',
            EnderecoEstado: '',
            EnderecoCEP: '',
            isDadosUpdate: false,
        };
    }

    componentDidMount = async () => {
        let token = await AsyncStorage.getItem("UserData");
        this.props.getAssociadoRequest(token)
        var dataFormatada = "";
        try {
            if (this.props.associado.data.DataNascimento) {
                var datePart = this.props.associado.data.DataNascimento.match(/\d+/g),
                    year = datePart[0], // get only two digits
                    month = datePart[1], day = datePart[2];
                dataFormatada = day + '/' + month + '/' + year;
            } else
                dataFormatada = ""
        } catch (error) {
            dataFormatada = ""
        }
        //console.log(dataFormatada, 'DATA FORMATADA', year)
        // const genderPlaceholder = numberValue=='male'?'masculino':'feminino'
        // //console.log(genderPlaceholder, 'GENERO PLACEHOLDER')
        var Sexo = "1";
        if (this.props.associado.data.Sexo)
            Sexo = this.props.associado.data.Sexo.toString();


        this.setState({
            token: token,
            Nome: this.props.associado.data.Nome,
            Sobrenome: this.props.associado.data.Sobrenome,
            DataNascimento: dataFormatada,
            Sexo: Sexo,
            CPF: this.props.associado.data.CPF,
            Email: this.props.associado.data.Email,
            Telefone: this.props.associado.data.Telefone,
            Celular: this.props.associado.data.Celular,
            TelefoneComercial: this.props.associado.data.TelefoneComercial,
            EmailAdicional: this.props.associado.data.EmailAdicional,
            EnderecoLogradouro: this.props.associado.data.EnderecoLogradouro,
            EnderecoNumero: this.props.associado.data.EnderecoNumero,
            EnderecoComplemento: this.props.associado.data.EnderecoComplemento,
            EnderecoBairro: this.props.associado.data.EnderecoBairro,
            EnderecoCidade: this.props.associado.data.EnderecoCidade,
            EnderecoEstado: this.props.associado.data.EnderecoEstado,
            EnderecoCEP: this.props.associado.data.EnderecoCEP,
        })
        console.log(this.state, 'DADOS PESSOAIS')
    }

    showActionSheet() {
        ActionSheetIOS.showActionSheetWithOptions({
            options: SEXO_BTNS,
            cancelButtonIndex: 2,
        },
            (buttonIndex) => {
                if (buttonIndex != 2)
                    this.setState({ Sexo: buttonIndex });
            });
    }

    componentWillMount() {

    }
    async componentWillReceiveProps(props) {
        if (props.associado.data.Sexo)
            this.setState({
                Sexo: props.associado.data.Sexo.toString()
            });

    }
    setModalVisibleAlertConcluido(visible) {
        this.setState({ modalVisibleAlertConcluido: visible });
        if (visible == false) {
            this.props.navigation.dispatch(resetAction)
            //console.log('dispatch')
        }
    }

    setModalVisibleAlertCancel(visible) {
        this.setState({ modalVisibleAlertCancel: visible });
    }


    onAtualizar = async () => {
        let dadosupdate = await AsyncStorage.getItem("IsDadosUpdate");

        this.props.getAtualizaRequest(this.state)

    }

    getCamera = async () => {
        let token = await AsyncStorage.getItem("UserData");
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {
                //console.log(response, 'RESPONSE FOTO')

                if (Platform.OS == 'ios') {
                    let uri = response.uri;
                    var obj = {
                        photo: uri,
                        namephoto: uri.substring(uri.lastIndexOf('/') + 1),
                        token: token,
                    };

                    this.setState(obj);
                    this.props.getAtualizaFotoRequest(obj)
                    this.props.getAssociadoRequest(this.state.token)

                } else {
                    this.setState({
                        photo: response.path,
                        namephoto: response.fileName,
                        token: token,
                    }, () => {
                        this.props.getAtualizaFotoRequest(this.state)
                        this.props.getAssociadoRequest(this.state.token)
                    })
                }



            }
        })
    }


    searchCep = (c) => {
        let r = c.replace("-", "");
        let cepFormatado = r;
        if (c.length == 9) {
            return axios.get('https://viacep.com.br/ws/' + cepFormatado + '/json')
                .then((response) => {
                    //console.log(response, 'RESPONSE CEP')
                    dataJson = response
                    this.setState({
                        EnderecoCEP: dataJson.data.cep,
                        EnderecoCidade: dataJson.data.localidade,
                        EnderecoEstado: dataJson.data.uf,
                        EnderecoBairro: dataJson.data.bairro,
                        EnderecoLogradouro: dataJson.data.logradouro,
                    })
                })
        } else {
            //console.log('não deu seis numero')
        }

    }


    render() {
        const { navigation } = this.props;

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.greenmosslight }} forceInset={{ bottom: 'never' }} >
                <Loading loading={this.props.atualizar.loading} />
                <View style={{ flex: 1, height: metrics.screenHeight }}>

                    <HeaderTitulo titulo="MEU PERFIL" navigation={this.props.navigation} />

                    <KeyboardAwareScrollView style={styles.containerFull} bounces={ifIphoneX(false)}>
                        <View style={styles.containerPerfil}>
                            <View style={styles.imageUser}>

                                {
                                    this.props.associado.data.CaminhoFoto === '' || this.props.associado.data.CaminhoFoto === null ?
                                        <Image
                                            source={require('../../assets/perfilpadrao.png')}
                                            style={
                                                styles.imgUsuario
                                            }
                                        /> :
                                        <Image
                                            source={{ uri: this.props.associado.data.CaminhoFoto }}
                                            style={
                                                styles.imgUsuario
                                            }
                                        />
                                }
                                {
                                    this.state.photo === null ?
                                        <Image
                                            source={{ uri: general.imagemPerfil + this.props.associado.data.Foto }}
                                            style={
                                                styles.imgUsuario
                                            }
                                        /> :
                                        <Image
                                            source={{ uri: 'file://' + this.state.photo }}
                                            style={
                                                styles.imgUsuario
                                            }
                                        />
                                }

                                <TouchableOpacity style={styles.altImg} onPress={() => { this.getCamera() }}>
                                    <Icon name="plus" style={styles.altIco} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.form}>
                                <Text style={styles.titleArea}>DADOS PESSOAIS</Text>
                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="user" style={{ marginLeft: 3 }} />
                                    <TextInput
                                        style={styles.camposInput}
                                        placeholder="nome"
                                        placeholderTextColor="#2F2F2F"
                                        value={this.state.Nome}
                                        onChangeText={Nome => this.setState({ Nome })}
                                    />
                                </View>

                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="user" style={{ marginLeft: 3 }} />
                                    <TextInput style={styles.camposInput} placeholder="sobrenome" placeholderTextColor="#2F2F2F"
                                        value={this.state.Sobrenome}
                                        onChangeText={Sobrenome => this.setState({ Sobrenome })}
                                    />
                                </View>

                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="birthday-cake" />
                                    <TextInputMask
                                        style={styles.camposInput}
                                        type={'datetime'}
                                        placeholder="data de nascimento"
                                        placeholderTextColor="#2F2F2F"
                                        options={{
                                            format: 'DD/MM/YYYY'
                                        }}
                                        value={this.state.DataNascimento}
                                        onChangeText={DataNascimento => this.setState({ DataNascimento })}
                                    />
                                </View>

                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="address-card" />
                                    <TextInputMask
                                        style={[styles.camposInput, {
                                            color: '#ccc',
                                        }]}
                                        type={'cpf'}
                                        placeholder="cpf"
                                        editable={false}
                                        placeholderTextColor="#2F2F2F"
                                        value={this.state.CPF}
                                        onChangeText={CPF => this.setState({ CPF })}

                                    />
                                </View>

                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="envelope" />
                                    <TextInput
                                        style={[styles.camposInput, {
                                            color: '#ccc',
                                        }]}
                                        placeholder="e-mail profissional" placeholderTextColor="#2F2F2F"
                                        editable={false}
                                        value={this.state.Email}
                                        onChangeText={Email => this.setState({ Email })} />
                                </View>
                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="envelope" />
                                    <TextInput
                                        style={styles.camposInput}
                                        placeholder="e-mail adicional" placeholderTextColor="#2F2F2F"
                                        value={this.state.EmailAdicional}
                                        onChangeText={EmailAdicional => this.setState({ EmailAdicional })} />
                                </View>

                                {
                                    Platform.OS == 'ios' ?
                                        <TouchableWithoutFeedback onPress={() => this.showActionSheet()}>
                                            <View style={{ width: '100%', height: 25, marginTop: 20, borderBottomWidth: 1, borderBottomColor: colors.graylight, flex: 1, flexDirection: 'row' }}>
                                                <Icon name="venus-mars" style={{ fontSize: 15, marginTop: -3 }} />
                                                <Text style={{ paddingLeft: 10, paddingBottom: 20 }}>{typeof SEXO_BTNS[this.state.Sexo] !== 'undefined' ? SEXO_BTNS[this.state.Sexo] : 'Escolha..'}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                        :
                                        <View style={{ width: '100%', height: 30, marginTop: 20, borderBottomWidth: 1, borderBottomColor: colors.graylight, }}>
                                            <Icon name="venus-mars" style={{ fontSize: 15, marginTop: -3 }} />
                                            <Picker
                                                selectedValue={this.state.Sexo}
                                                style={styles.camposSelect}
                                                onValueChange={(value) => {
                                                    this.setState({ Sexo: value })
                                                }}>
                                                <Picker.Item label="Masculino" value="1" />
                                                <Picker.Item label="Feminino" value="2" />
                                            </Picker>
                                        </View>
                                }


                                {/* <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="phone" />
                                    <TextInputMask
                                        style={styles.camposInput}
                                        type={'cel-phone'}
                                        placeholder="telefone residencial"
                                        placeholderTextColor="#2F2F2F"
                                    />
                                </View> */}
                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="phone-square" />
                                    <TextInputMask
                                        style={styles.camposInput}
                                        type={'cel-phone'}
                                        placeholder="telefone residencial"
                                        placeholderTextColor="#2F2F2F"
                                        value={this.state.Telefone}
                                        onChangeText={Telefone => this.setState({ Telefone })}
                                    />
                                </View>
                                {/*                         
                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="phone-square" />
                                    <TextInputMask
                                        style={styles.camposInput}
                                        type={'cel-phone'}
                                        placeholder="telefone comercial"
                                        placeholderTextColor="#2F2F2F"
                                        value={this.state.TelefoneComercial}
                                        onChangeText={TelefoneComercial => this.setState({ TelefoneComercial })}
                                    />
                                </View> */}

                                <View style={{ width: '100%', marginTop: 20 }}>
                                    <Icon name="mobile" style={{ fontSize: 20, marginLeft: 3 }} />
                                    <TextInputMask
                                        style={styles.camposInput}
                                        type={'cel-phone'}
                                        placeholder="celular"
                                        placeholderTextColor="#2F2F2F"
                                        value={this.state.Celular}
                                        onChangeText={Celular => this.setState({ Celular })}
                                    />
                                </View>

                            </View>
                        </View>

                        <View style={stylesEndereco.containerPerfil}>
                            <View style={stylesEndereco.icone}>
                                <Icon name="map-marker" size={18} />
                            </View>
                            <View style={stylesEndereco.form}>
                                <Text style={styles.titleArea}>ENDEREÇO</Text>

                                <View style={{ width: '100%', }}>
                                    <TextInputMask
                                        style={stylesEndereco.camposInput}
                                        type={'zip-code'}
                                        placeholder="cep"
                                        placeholderTextColor="#2F2F2F"
                                        options={{
                                            format: '99.999-999'
                                        }}

                                        value={this.state.EnderecoCEP}
                                        onChangeText={this.searchCep.bind(this)}
                                    />
                                </View>

                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="logadouro" placeholderTextColor="#2F2F2F"
                                        value={this.state.EnderecoLogradouro}
                                        onChangeText={EnderecoLogradouro => this.setState({ EnderecoLogradouro })} />
                                </View>

                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="número" placeholderTextColor="#2F2F2F"
                                        value={this.state.EnderecoNumero}
                                        onChangeText={EnderecoNumero => this.setState({ EnderecoNumero })} />
                                </View>

                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="complemento" placeholderTextColor="#2F2F2F"
                                        value={this.state.EnderecoComplemento}
                                        onChangeText={EnderecoComplemento => this.setState({ EnderecoComplemento })} />
                                </View>

                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="bairro" placeholderTextColor="#2F2F2F"
                                        value={this.state.EnderecoBairro}
                                        onChangeText={EnderecoBairro => this.setState({ EnderecoBairro })}
                                    />
                                </View>

                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="cidade" placeholderTextColor="#2F2F2F"

                                        value={this.state.EnderecoCidade}
                                        onChangeText={EnderecoCidade => this.setState({ EnderecoCidade })} />
                                </View>
                                <View style={{ width: '45%', }}>
                                    <TextInput style={stylesEndereco.camposInput} placeholder="estado" placeholderTextColor="#2F2F2F"

                                        value={this.state.EnderecoEstado}
                                        onChangeText={EnderecoEstado => this.setState({ EnderecoEstado })} />
                                </View>
                            </View>

                            <View style={styles.buttonContainer}>
                                <TouchableOpacity activeOpacity={0.8} style={styles.buttonEntrar}
                                    onPress={() => {
                                        this.onAtualizar();
                                    }}>
                                    <Text style={styles.txtButton}> SALVAR </Text>
                                </TouchableOpacity>
                            </View>

                            {/* <Text style={styles.cancelAc}>
                                Deseja cancelar sua conta?
                        <Text style={styles.cancelAcBold}
                                    onPress={() => {
                                        this.setModalVisibleAlertCancel(true);
                                    }}> Clique aqui.</Text>
                            </Text> */}
                        </View>

                        <Alerts visible={this.props.atualizar.alert} icon={this.props.atualizar.error ? 'times' : 'check'}
                            mensagem={
                                this.props.atualizar.error
                                    ?
                                    this.props.atualizar.error
                                    :
                                    this.props.atualizar.data.message
                                        ? this.props.atualizar.data.message.toUpperCase()
                                        : 'DADOS SALVOS COM SUCESSO!'
                            }
                            closeAction={() => {
                                this.props.doAtualizaCloseModal()
                                this.props.navigation.goBack();
                            }} />




                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisibleAlertCancel}
                            onRequestClose={() => {
                                this.setModalVisibleAlertCancel(false)
                            }}>
                            <ImageBackground
                                style={styles.imgBackground}
                                resizeMode='cover'
                                source={require('../../assets/bg.png')}
                            >
                                <View style={styles.allModal}>
                                    <Text style={styles.iconTimes}
                                        onPress={() => this.setModalVisibleAlertCancel(false)}>
                                        <Icon name="times" size={25}
                                            style={styles.iconClose} />
                                    </Text>
                                    <View style={styles.containerModal}>
                                        <Text style={styles.titleModalAvise}>EXCLUIR CADASTRO?</Text>
                                        <Text style={styles.textModalAvise}>
                                            Ao deixar de fazer parte do programa todos os seus pontos serão excluídos e você perderá todas as vantagens que o programa oferece. Não será possível retornar com sua conta posteriormente. Deseja confirmar?</Text>
                                        <View style={styles.containerBtns}>
                                            <TouchableOpacity activeOpacity={0.8} style={styles.btnNo}
                                                onPress={() => {
                                                    this.setModalVisibleAlertCancel(false);
                                                }}>
                                                <Text style={styles.txtBtns}>NÃO</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity activeOpacity={0.8} style={styles.btnYes}
                                                onPress={() => {
                                                    this.setModalVisibleAlertCancel(false);
                                                }}>
                                                <Text style={styles.txtBtns}>SIM</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </Modal>

                    </KeyboardAwareScrollView>
                </View>
            </SafeAreaView>

        );
    }
}


const mapStateToProps = state => ({
    associado: state.associado,
    atualizar: state.atualizar,
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        ...AssociadosActions,
        ...AtualizarActions,
    }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(meuPerfilDadosPessoais);