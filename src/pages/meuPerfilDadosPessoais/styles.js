import {  StyleSheet, Platform } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';

const styles = StyleSheet.create({
  header: {
      width: '100%',
      backgroundColor: colors.greenmosslight,
      textAlign: 'center',
  },
  containerHeader: {
    width: metrics.defaultWidthPag,
    paddingTop: Platform.OS == 'ios' ? 15 : 0,
    height: Platform.OS=="ios" ? 70 : 45,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'space-between'
  },
  title: {
      color: colors.white,
      fontWeight: 'bold',
      fontSize: metrics.defaultFontSizeTxt,
      textAlign: 'center',
      width: '95%'
  },
  imgUsuario: {
    position: 'absolute',
    height: 116,
    width: 116,
    borderRadius: 58,
    marginTop: 20,
    borderWidth: 6,
    borderColor: colors.greenmoss,
},
  btnIconBack: {
      height: 45,
      width: '5%',
      lineHeight: 45
  },
  iconBack: {
      color: '#ffffff',
      fontSize: 21,
      lineHeight: 45
  },
  menu: {
    width: '100%',
    backgroundColor: '#000000',
    paddingTop: 25,
    paddingBottom: 25,
  },
  container: {
    width: metrics.defaultWidthPag,
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    flexWrap: 'wrap'
  },
  containerPerfil: {
    width: metrics.defaultWidthPag,
    justifyContent: 'flex-start',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  btnDestaque: {
    width: '50%',
    backgroundColor: '#B9B9B9',
    minHeight: 25,
    borderRadius: 50
  },
  btnSemDestaque: {
    width: '50%',
    minHeight: 25
  },
  txtDestaque: {
    width: '100%',
    color: '#ffffff',
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: 'center',
    lineHeight: 25,
    fontWeight: 'bold'
  },
  txtSemDestaque: {
    width: '100%',
    color: '#B9B9B9',
    fontSize: metrics.defaultFontSizeTxt,
    textAlign: 'center',
    lineHeight: 25,
    fontWeight: 'bold'
  },
  imageUser: {
    width: 110,
    height: 110,
    position: 'relative',
    marginTop: 30,      
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 30,
  },
  imageItem: {
    width: 110,
    height: 110,
    position: 'absolute',
  },
  altImg: {
      backgroundColor: colors.yellow,
      height: 30,
      width: 30,
      lineHeight: 30,
      textAlign: 'center',
      borderRadius: 20,
      position: 'absolute',
      bottom: -25,
      right: 10,
  },
  altIco: {
    color: colors.white,
    height: 30,
    width: 30,
    lineHeight: 30,
    textAlign: 'center',
  },
  form: {
      width: '100%'
  },
  camposInput: {
    height: 50,
    width: "100%",
    borderBottomWidth: 1,
    color: colors.gray,
    borderBottomColor: colors.graylight,
    paddingLeft: 25,
    marginTop: -32,
    height: 50
  },
  camposSelect: {
    width: "100%",
    borderBottomWidth: 1,
    color: colors.gray,
    borderBottomColor: colors.graylight,
    paddingLeft: 25,
    marginTop: -32,
    height: 50,
    width: '100%',
    transform: [
        { scaleX: 0.9 }, 
        { scaleY: 0.9 },
     ]
  },
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: '#cf9112',
    width: 280,
    height: 35,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30
  },
  buttonContainer: {
    width: '100%',
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20
  },
  txtButton: {
    color: '#fff',
    fontSize: metrics.defaultFontSizeTxt,
    fontWeight: 'bold'
  },
  cancelAc: {
    fontSize: metrics.defaultFontSizeTxt,
    color: colors.gray,
    textAlign: 'center',
    marginBottom: 30,
    marginTop: 10,
    width: '100%'
  },
  cancelAcBold: {
    fontWeight: 'bold'
  },
  titleArea: {
    textAlign: 'center',
    fontSize: metrics.defaultFontSizeTitle,
    fontWeight: 'bold',
    width: '100%',
    marginTop: 15
  },
  
  imgBackground: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerModal: {
    width: metrics.defaultWidthModal,
    backgroundColor: '#ffffff',
    padding: 20,
    borderRadius: 8
  },
  btnFechar: {
    height: 35,
    width: '100%',
    backgroundColor: '#019e35',
    borderRadius: 50,
    marginTop: 15
  },
  iconSuccess: {
      marginLeft: 'auto',
      marginRight: 'auto',
      marginBottom: 10
  },
  iconTimes: {
      width: '100%',
      textAlign: 'right',
      position: 'absolute',
      top: 10,
      color: colors.yellow,
      fontWeight: 'bold',
      paddingRight: 20
  },
  allModal: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
  },
  txtBtns: {
    width: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#ffffff',
    lineHeight: 35
  },
  titleModalAvise: {
    fontSize: metrics.defaulth2,
    width: '100%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  btnNo: {
    height: 35,
    width: '40%',
    borderRadius: 50,
    backgroundColor: colors.yellow,
    marginTop: 15,
    justifyContent: 'center'
  },
  btnYes: {
    height: 35,
    width: '40%',
    backgroundColor: '#019e35',
    borderRadius: 50,
    marginTop: 15,
    justifyContent: 'center'
  },
  containerBtns: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textModalAvise: {
    marginTop: 15,
    marginBottom: 10
  },
  containerFull: {
    backgroundColor: colors.white,
  }
});

export default styles;
  