import React, { Component, PureComponent } from 'react';
import {
  TouchableOpacity,
  Text, View,
  Image,
  TextInput,
  ImageBackground,
  AsyncStorage,
  NetInfo,
  ActivityIndicator,
  ScrollView,
  BackHandler,
  Modal
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { ifIphoneX } from 'react-native-iphone-x-helper';

import { Container, Header, Content, Footer, FooterTab } from 'native-base';

import { connect } from 'react-redux';
import { Creators as LoginActions } from '../../store/ducks/login';
import Icon from 'react-native-vector-icons/FontAwesome';
import Alert from '../../components/Alert';

import { bindActionCreators } from 'redux';
import { facebookLogin } from '../../services/auth';
import styles from './styles';
import Loader from '../../components/loading';
import Modais from '../../components/modal';


import { navigatorRef } from "../../App";
import { StackActions, NavigationActions,withNavigationFocus } from "react-navigation";
import metrics from '../../styles/metrics';

class Login extends React.Component {



  constructor(props){
    super(props)
    this.state = {
      email: '',
      password: '',
      error: '',
      modalVisible: false,
      EmailNaoPreenchido: false,
      semconexao: false,
    }
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      //console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
      
      if(connectionInfo.type=='none'){
        this.setState({
          semconexao: true
      })
      }
    

  });
  }

  handleLoginWithFacebook = async () => {
    // const { getLoginFBRequest } = this.props;
    const response = await facebookLogin();
    if (response.error) {
      this.setState({ error: response.error });
      //console.log(response.error);
      return false;
    }
    //console.log(response, 'INFOS FACE');
    this.props.getLoginFBRequest(response);
  } 

  validation = () => {
    if (this.state.email=='' && this.state.password=='') {
      return this.setState({
          EmailNaoPreenchido: true
        })

    } 
    return true
}

  handleLogin = async () => {
    let mensagem = this.validation()
    if(this.state.semconexao==true){
      this.props.navigation.navigate('semInternet')
      }else if (mensagem == true && this.state.semconexao==false) {
    this.props.getLoginRequest(this.state);
    }
  }
   
  


  handleBackButton() {
      return true;
  }

  fecharModal() {
    this.props.closeModal();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }


  render() {

    const { navigation, login,isFocused } = this.props;
    if (isFocused)
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    else
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);




    return (
      <View>

        <Alert 
          visible={this.state.EmailNaoPreenchido}
          icon="exclamation"
          mensagem={'VOCÊ PRECISA INFORMAR SEU \nE-MAIL E SENHA.'} 
          closeAction={() =>  this.setState({
            EmailNaoPreenchido: false
          }) }/>

        <Alert 
          visible={this.props.login.error}
          icon="exclamation"
          mensagem={this.props.login.error_message.toUpperCase()} 
          closeAction={() =>   this.fecharModal()  }/>
      

              
        <KeyboardAwareScrollView contentContainerStyle={{ height: metrics.screenHeight }} bounces={ifIphoneX(false)}>

          <ImageBackground style={styles.imgBackground}
            resizeMode='cover'
            source={require('../../assets/bgEscuro.png')}>

          

            <View style={styles.container}>
              <Loader 
                loading={this.props.login.loading} 
                />

              
              <View style={{ flexDirection:'column', justifyContent: 'flex-end', alignContent: 'flex-end',flexGrow:0.2}}>
                <Image
                  source={require('../../assets/logobranca.png')}
                  style={
                    styles.logo
                  }
                />
              </View>
              <Text style={styles.ptopo}>Seja bem-vindo! Tenha na palma da sua mão informações, promoções e descontos exclusivos preparados especialmente para os colaboradores das empresas parceiras do programa.</Text>
              <View style={{width: '80%', alignSelf: 'center'}}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.buttonRedeS}
                  onPress={this.handleLoginWithFacebook}>
                  
              
                <Icon style={styles.iconfb} name="facebook-f" size={20}/>
              <Text style={styles.txtButton}>ENTRAR COM FACEBOOK</Text>
             
              </TouchableOpacity>

              <View style={{width: '100%' ,marginTop: 18, flexDirection:'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={styles.barraOu}></View>
                <Text style={styles.textoOu}> OU </Text>
                <View style={styles.barraOu}></View>
              </View>

                 <Text style={styles.txtou}></Text>
                <View style={{ width: '100%', marginTop: -20}}>
                    <Icon name="envelope" size={14} color="#fff" />                
                    <TextInput
                      returnKeyType="next"
                      onSubmitEditing={()=>{this.ipt_password.focus();}} 
                      style={styles.camposInput}
                      value={this.state.email}
                      placeholder="e-mail profissional"
                      autoCapitalize="none"
                      keyboardType="email-address"
                      autoCorrect={false}
                      textContentType="emailAddress"
                      placeholderTextColor="#fff"
                      onChangeText={email => this.setState({ email })}
                    />
                </View>


                <View style={{ width: '100%', marginTop: 25 }}>
                  <Icon name="lock" size={16} color="#fff" />   
                  <TextInput
                    ref={(input) => { this.ipt_password = input; }}
                    returnKeyType="go"
                    textContentType="password"
                    onSubmitEditing={() => this.handleLogin()}
                    secureTextEntry={true}
                    style={styles.camposInput}
                    placeholder="senha"
                    autoCapitalize="none"
                    returnKeyLabel={'next'}
                    autoCorrect={false}
                    value={this.state.password}
                    placeholderTextColor="#fff"
                    onChangeText={password => this.setState({ password })} />
                </View>

              <Text style={styles.txtEsquecisenha} onPress={() => { navigation.navigate('esqueciSenha') }}>ESQUECI MINHA SENHA</Text>
              </View>    
              <View style={styles.buttonContainer}>

                <TouchableOpacity activeOpacity={0.8} style={styles.buttonEntrar} onPress={() => { this.handleLogin() }}>
                  <Text style={styles.txtButton} > ENTRAR </Text>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8} style={[styles.footer, ifIphoneX(styles.footerIphoneX)]} onPress={() => { navigation.navigate('cadastro') }}>
                  <Text style={styles.txtFooter}>
                    NÃO POSSUI UMA CONTA?<Text style={styles.boldTextStyle}> CADASTRE-SE!</Text>
                  </Text>
                </TouchableOpacity>

              </View>
            </View>

          </ImageBackground>
        </KeyboardAwareScrollView>

      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(LoginActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)( withNavigationFocus(Login));

