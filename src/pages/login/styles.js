import { StyleSheet } from 'react-native';
import metrics from '../../styles/metrics';
import { Dimensions, Platform } from 'react-native';
import colors from '../../styles/colors';
import { ifIphoneX } from 'react-native-iphone-x-helper';


const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: Platform.OS == 'ios' ? 'space-around' : 'space-between',
    paddingTop: Platform.OS == 'ios' ? ifIphoneX(60) : null,
    height: width < height ? height : width,
    width: width < height ? width : height,
  },
  containerScroll: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconAlert: {
    fontSize: 45,
    marginTop: 30,
    color: '#000000'
  },
  modalContent: {
    position: 'relative',
  },
  modalContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtAlertCont: {
    fontWeight: 'bold',
    flexWrap: 'wrap',
    width: '85%',
    textAlign: 'center',
    paddingTop: 30,
    paddingBottom: 10,
    fontSize: metrics.defaultFontSizeTitle
  },
  buttonVoltar: {
    position: 'absolute',
    top: 15,
    right: 15,
    zIndex: 99
  },
  fbzin: {
    width: 15,
    height: 15
  },
  alertCont: {
    backgroundColor: '#ffffff',
    width: metrics.defaultWidthModal,
    alignItems: 'center',
    paddingBottom: 30,
    borderRadius: 5
  },
  txtButtonVoltar: {
    color: colors.yellow,
    fontWeight: 'bold',
    fontSize: 24
  },
  imgBackground: {
    width: width,
    height: height,
    flex: 1,
  },
  logo: {
    width: Platform.OS == 'ios' ? 220 : 250,
    paddingBottom: 0,
    resizeMode: 'contain',
    marginTop: 10,
  },
  camposInput: {
    height: 50,
    width: "100%",
    borderBottomWidth: 1,
    color: '#fff',
    fontSize: 16,
    borderBottomColor: '#fff',
    paddingLeft: 25,
    marginTop: -33,
  },
  boldTextStyle: {
    fontWeight: 'bold',
  },
  txtFooter: {
    fontSize: Platform.OS == 'ios' ? 12 : 14
  },
  iconfb: {
    color: colors.white,    
    marginRight: 5
  },
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '80%',
    height: Platform.OS == 'ios' ? 30 : metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: Platform.OS == 'ios' ? ifIphoneX(20) : 10
  },
  buttonEntrar2: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '80%',
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  botaomodal: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: 250,
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  buttonEntrarFB: {
    borderRadius: 100,
    backgroundColor: '#0f5084',
    width: 320,
    height: metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  buttonRedeS: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '100%',
    height: Platform.OS == 'ios' ? 30 : metrics.heightButton,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    flexDirection: 'row'
  },
  buttonContainer2: {
    width: metrics.defaultWidthPag,
    flexDirection: "row",
    justifyContent: "center"
  },
  buttonContainer: {
    width: '100%',
    flexDirection: "column",
    justifyContent: "center",
    alignItems: 'center',
  },
  ptopo: {
    color: '#fff',
    fontFamily: 'Open Sans',
    fontSize:  Platform.OS == 'ios'? metrics.fontpIos : 17,
    paddingLeft: 15,
    paddingRight: 15,  
    flexDirection: "row",
    textAlign: 'center',
    justifyContent: "center",
  },
  txtButton: {    
    color: '#fff',
    fontFamily: 'Open Sans',
    fontSize: Platform.OS == 'ios' ? 12 : 16,
    fontWeight: 'bold',
  },
  txtButtonFb: {
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    fontFamily: 'Open Sans'
  },
  txtou: {
    marginTop: 18,
    color: '#fff',
    fontFamily: 'Open Sans',
    fontSize: 15
  },
  barraOu: {
    backgroundColor: colors.yellow,
    height:1,
    flexGrow: 1 
  },
  textoOu: {
    color: colors.yellow,
    fontFamily: 'Open Sans',
    fontSize: 12,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 0
  },
  footer: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingTop: 20,
    paddingBottom: 20,
    height: Platform.OS == 'ios' ? ifIphoneX(70) : 65,
    width: '100%',
    marginBottom: Platform.OS == 'ios' ? ifIphoneX(-30) : 0,
  },
  footerIphoneX:{
    height: 95,
    paddingBottom: 55
  },
  txtfooter: {
    color: '#333',
    fontSize: 17,
    fontFamily: 'Open Sans',
  },
  txtEsquecisenha: {
    color: '#fff',
    marginTop: 20,
    marginBottom: 1,
    fontSize: 13,
    fontFamily: 'Open Sans',
    alignSelf: 'center'
  },
});

export default styles;