import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';
import metrics from '../../styles/metrics';

const styles = StyleSheet.create({

  mgTop : { marginTop: 200 },
  slide1: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnPequeno: {
    padding: 15
  },
  slide2: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  slide3: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  slide4: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'center'
  },
  txtbold:{
    fontFamily: 'Open Sans',
    textAlign: 'center',
    fontSize: 18,
    justifyContent: 'center',
    alignItems: 'center',
    color: colors.white,
    fontWeight: 'bold',
  },
  buttonEntrar: {
    borderRadius: 100,
    backgroundColor: colors.yellow,
    width: '85%',
    height: metrics.heightButton,
    color: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  text: {
    color: colors.white,
    fontSize: 28,
    fontWeight: 'bold',
  },
  logo: {
    position: 'absolute',
    top: 30,
    width: 90,
    height: 90
  },
  ptxt: {
    fontFamily: 'Open Sans',
    textAlign: 'center',
    fontSize: 18,
    justifyContent: 'center',
    alignItems: 'center',
    color: colors.white,
  },
  h1white: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 28,
    // marginTop: 280,
    fontFamily: 'Open Sans',
    width: '100%',
    textAlign: 'center'
  },
  h1whiteslide1: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 28,
    // marginTop: 240,
    fontFamily: 'Open Sans',
    width: '100%',
    textAlign: 'center'
  },
  h1whiteslide2: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 28,
    // marginTop: 250,
    fontFamily: 'Open Sans',
    width: '100%',
    textAlign: 'center'
  },
  h1whiteslide3: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 32,
    // marginTop: 270,
    marginTop: 30,
    fontFamily: 'Open Sans',
    width: '100%',
    textAlign: 'center'
  },
  h1whiteslide4: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 28,
    // marginTop: 280,
    marginTop: 30,
    fontFamily: 'Open Sans',
    width: '100%',
    textAlign: 'center'
  },
  h1whitemtopzin: {
    fontWeight: '100',
    color: colors.white,
    fontSize: 28,
    marginTop: 220
  },
  h1yellow: {
    fontSize: 32,
    color: colors.yellow,
    fontWeight: 'bold',
    marginTop: -6,
    marginBottom: 20,
  },

  txtButton: {
    color: colors.white,
    fontSize: metrics.fontButton,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  pular: {
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    height: 60,
  },
  setaimg: {
    width: 12,
    height: 18
  },
  dot: {
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.white
  },
  activeDotColor: {
    backgroundColor: colors.yellow
  }
})




export default styles;