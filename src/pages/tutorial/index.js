import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  AsyncStorage,
  View,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

import { StackActions, NavigationActions } from "react-navigation";
import { navigatorRef } from "../../App";

class Tutorial extends Component {

  nextPage = async () => {
    await AsyncStorage.setItem('getFirst', 'true');
    const nav = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'LoginStack'
        })
      ],
      key: null
    });
    navigatorRef.dispatch(nav);
  }
  componentWillReceiveProps(props) {
    this.setState(this.initState(props));
    if (props.yourNewPageIndex) {
      this.scrollBy(props.yourNewPageIndex)
    }
  }

  onSwipe = (index) => {
    this.setState({
      sliderLength: index
    })
    //console.log('index changed', index);
  }

  render() {
    const { navigation } = this.props;
    return (

      <Swiper ref='swiper' showsButtons={false} loop={false} dotStyle={styles.dot} activeDotStyle={styles.activeDotColor} >


        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../../assets/bgtuto1.png')}>

          <View style={[styles.slide1, styles.mgTop]} >
            
            <Text style={styles.h1whiteslide1}>FAÇA SEU</Text>
            <Text style={styles.h1yellow}>CADASTRO</Text>

            <Text style={styles.ptxt}>E tenha na palma da mão todas as novidades do <Text style={{fontWeight:'bold'}}>Conexão Eldorado</Text>. Para se cadastrar, utilize seu <Text style={{fontWeight:'bold'}}>e-mail profissional</Text>.</Text>

            <View style={styles.pular}>
              <TouchableOpacity style={styles.btnPequeno} onPress={this.nextPage}>
                <Text style={styles.ptxt}>pular</Text>
              </TouchableOpacity>

              
              <TouchableOpacity style={styles.btnPequeno} onPress={() => this.refs.swiper.scrollBy(1)}>
                <Icon name="arrow-right" size={20} color="#fff"  />
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
        
        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../../assets/bgtuto2.jpg')}>
          
          <View style={[styles.slide2, styles.mgTop]}>

            <Text style={styles.h1whiteslide2}>FAÇA</Text>
            <Text style={styles.h1yellow}>COMPRAS</Text>

            <Text style={styles.ptxt}>Todas as suas compras geram <Text style={{fontWeight:'bold'}}>pontos</Text> no programa. Não esqueça de <Text style={{fontWeight:'bold'}}>pedir nota fiscal</Text> e lembre-se de informar seu <Text style={{fontWeight:'bold'}}>CPF</Text>.</Text>

            <View style={styles.pular}>
              
              <TouchableOpacity style={styles.btnPequeno} onPress={this.nextPage}>
                <Text style={styles.ptxt}>pular</Text>
              </TouchableOpacity>

              
              <TouchableOpacity style={styles.btnPequeno} onPress={() => this.refs.swiper.scrollBy(1)} >
                <Icon name="arrow-right" size={20} color="#fff" />
              </TouchableOpacity>
            </View>

          </View>

        </ImageBackground>

        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../../assets/bgtuto3.jpg')}>
          
          <View style={[styles.slide3, styles.mgTop]}>

            <Text style={styles.h1whiteslide3}>ACUMULE</Text>
            <Text style={styles.h1yellow}>PONTOS</Text>

            <Text style={styles.ptxt}>Cadastre todas as Notas Fiscais de compras realizadas nas lojas do Shopping Eldorado.</Text>
            <Text style={styles.txtbold}>Elas valem pontos que podem ser trocados por descontos e vantagens.</Text>

            <View style={styles.pular}>
              <TouchableOpacity style={styles.btnPequeno} onPress={this.nextPage}>
                <Text style={styles.ptxt}>pular</Text>
              </TouchableOpacity>

              
              <TouchableOpacity style={styles.btnPequeno} onPress={() => this.refs.swiper.scrollBy(1)} >
                <Icon name="arrow-right" size={20} color="#fff"  />
              </TouchableOpacity>
            </View>
            
          </View>
        </ImageBackground>

        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../../assets/bgtuto4.png')}>
          <View style={[styles.slide4, styles.mgTop]}>

            <Text style={styles.h1whiteslide4}>BENEFÍCIOS</Text>
            <Text style={styles.h1yellow}>RECOMPENSAS</Text>

            <Text style={styles.ptxt}>Resgate as recompensas e <Text style={{fontWeight:'bold'}}>aproveite os benefícios preparados</Text> especialmente para você!</Text>
            <TouchableOpacity style={styles.buttonEntrar} onPress={this.nextPage}>
              <Text style={styles.txtButton}> ACESSAR O APLICATIVO </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </Swiper>
    );
  }
}

export default Tutorial 