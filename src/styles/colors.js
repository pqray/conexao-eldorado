const colors = {
    white: '#FFFFFF',
    black: '#000000',
    gray: '#2F2F2F',
    graylight: '#B9B9B9',
    grayrow: '#E0E0E0',
    yellow: '#cf9112',
    bgButton: '#cf9112',
    bgTitle: '#9C936B',
    yellowp: '#cf9112',
    bgContainerFull: '#F5F5F5',
    yellowp: '#EFB517',
    greenmoss: '#57563D',
    greenmosslight: '#9C936B',
    greenUser: '#3E3E2D',
    btResgate: '#4D602E',
    btAviseMe: '#E4A119',
    btnYes: '#019e35',
    bgPag: '#F3F3F3'
};

export default colors;