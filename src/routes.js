import { createStackNavigator, StackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { createDrawerNavigator, DrawerNavigator, createSwitchNavigator } from 'react-navigation';
import { fromTop,fromLeft,fadeIn,zoomIn } from 'react-navigation-transitions';
import React from 'react';
import { Image, Alert, Text, View, Button, TouchableOpacity } from 'react-native';
import Tutorial from './pages/tutorial';
import Splash from './pages/splash';
import Login from './pages/login';
import esqueciSenha from './pages/esqueciSenha';
import EsqueciSenhaConfirmacao from './pages/esqueciSenhaConfirmacao';
import termosUso from './pages/termosUso';
import cadastro from './pages/cadastro';
import cadastroConfirmacao from './pages/cadastroConfirmacao';
import pontosAdquiridos from './pages/pontosAdquiridos';
import dadosSalvos from './pages/dadosSalvos';

import DrawerSidebar from './components/DrawerSidebar';
import TabBottomBar from './components/TabBottomBar';

import duvidasFrequentes from './pages/duvidasFrequentes';
import duvidasFrequentesInterna from './pages/duvidasFrequentesInterna';
import verificaVersao from './pages/verificaVersao';
import semInternet from './pages/semInternet';

import Home from './pages/home';
import Regulamento from './pages/Regulamento';
import notificacao from './pages/notificacao';
import notificacaoInterna from './pages/notificacaoInterna';
import meusPontos from './pages/meusPontos';
import recompensasDisponiveis from './pages/recompensasDisponiveis';
import meuPerfilDadosPessoais from './pages/meuPerfilDadosPessoais';
import meuPerfilSenha from './pages/meuPerfilSenha';
import historico from './pages/historico';
import mensagens from './pages/mensagens';
import meuPerfil from './pages/meuPerfil';
import cadastrarNota from './pages/cadastrarNota';
import recompensaInterna from './pages/recompensaInterna';
import lojasInterna from './pages/lojasInterna';
import lojas from './pages/lojas';
import enviarMensagem from './pages/enviarMensagem';
import FotoNota from './pages/cadastrarNota/foto';
import QRCode from './pages/cadastrarNota/qrcode';

import colors from './styles/colors';
import esqueciSenhaConfirmacao from './pages/esqueciSenhaConfirmacao';
import Inicial from './pages/inicial';
// import PosFoto from './pages/posfoto';

import { FluidNavigator, Transition } from  'react-navigation-fluid-transitions';

const defaultTransaction = fromTop;

const RecompensaStackNavigator = FluidNavigator({
  Recompensa: { screen: recompensasDisponiveis },
  RecompensaInterna: { screen: recompensaInterna },
}, {
    headerMode: "none",
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.white,
        borderBottomColor: 'transparent',
      },
      headerTintColor: colors.black,
      headerBackTitle: null,
    },
  });

const HomePStackNavigator = FluidNavigator({
  Home: { screen: Home },
  RecompensaInterna: { screen: recompensaInterna },
}, {
    headerMode: "none",
    navigationOptions: {
      headerStyle: {
        backgroundColor: colors.white,
        borderBottomColor: 'transparent',
      },
      headerTintColor: colors.black,
      headerBackTitle: null,
    },
  });

const CadastrarNotaStack = createStackNavigator({
  cadastrarNota: {
    screen: cadastrarNota
  },
  QRCode: {
    screen: QRCode,
  },
  FotoNota: {
    screen: FotoNota,
  }
},
{
  transitionConfig: () => defaultTransaction(),  
  headerMode: "none",
  navigationOptions: {
    headerStyle: {
      backgroundColor: colors.white,
      borderBottomColor: 'transparent',
    },
    headerTintColor: colors.black,
    headerBackTitle: null,
  },
})

LojasStack = createStackNavigator({
  lojas: { screen: lojas },
  lojasInterna: { screen: lojasInterna },
}, {
  headerMode: "none",
  navigationOptions: {
    headerStyle: {
      backgroundColor: colors.white,
      borderBottomColor: 'transparent',
    },
    headerTintColor: colors.black,
    headerBackTitle: null,
  },
}
);

LojasStack.navigationOptions = {
  transitionConfig: () => defaultTransaction(),  
  tabBarLabel: 'BENEFÍCIOS',
  tabBarIcon: ({ focused, tintColor }) => (
    focused ?
      <Image source={require('./assets/lojas-active.png')} style={{ width: 24, height: 24 }} />
      :
      <Image source={require('./assets/lojas.png')} style={{ width: 24, height: 24 }} />
  ),
}
// Configurações da TabBar

const TabBarMain = createBottomTabNavigator({
  Home: {
    screen: HomePStackNavigator
  },
  MeusPontos: {
    screen: meusPontos
  },
  cadastrarNota: {
    screen: CadastrarNotaStack,
    navigationOptions:{
      transitionConfig: () => defaultTransaction(),  
      tabBarLabel: 'NOTAS',
      tabBarIcon: ({ focused,tintColor }) => (
          focused ? 
              <Image source={require('./assets/notas-active.png')}
              style={{ width: 21, height: 24 }}/>
              :
              <Image source={require('./assets/notas.png')} 
              style={{ width: 21, height: 24 }}/>
          ),
    },
  },
  Recompensas: {
    screen: RecompensaStackNavigator,
    navigationOptions: {
      transitionConfig: () => fromLeft(),

    }
  },
  Lojas: {
    screen: LojasStack,
    navigationOptions: {
      transitionConfig: () => fromLeft(),
    }
  },
},
  {
    transitionConfig: () => defaultTransaction(),
    animationEnabled:false, 
    tabBarComponent: TabBottomBar,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: colors.gray,
      inactiveTintColor: '#B9B9B9',
      labelStyle: {
        fontSize: 8,
      },
      style: {
        backgroundColor: colors.white,
        borderTopColor: '#000',
        fontSize: 13,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'red',
        shadowOffset: { height: 0, width: 0 },
      },
    },
  });

RecompensaStackNavigator.navigationOptions = {
  transitionConfig: () => defaultTransaction(),  
  tabBarLabel: 'RECOMPENSAS',
  tabBarIcon: ({ focused, tintColor }) => (
    focused ?
      <Image source={require('./assets/recompensas-active.png')} style={{ width: 24, height: 24 }} />
      :
      <Image source={require('./assets/recompensas.png')} style={{ width: 24, height: 24 }} />
  ),
}
HomePStackNavigator.navigationOptions = {
  tabBarLabel: 'INÍCIO',
  tabBarIcon: ({ focused, tintColor }) => (
    focused ?
      <Image source={require('./assets/home-active.png')} style={{ width: 21, height: 24 }} />
      :
      <Image source={require('./assets/home.png')} style={{ width: 21, height: 24 }} />
  ),
}



const HomeStackNavigator = createStackNavigator({
  TabBarMain: { screen: TabBarMain },
  // BeneficioInterna: { screen: recompensasDisponiveis },
  recompensaInterna: { screen: recompensaInterna },
  verificaVersao: { screen: verificaVersao }

}, {
    headerMode: "none",
    transitionConfig: () => defaultTransaction(),
    navigationOptions: {
      
      headerStyle: {
        backgroundColor: colors.white,
        borderBottomColor: 'transparent',
      },
      headerTintColor: colors.black,
      headerBackTitle: null,
    },
  });






LoginStack = createStackNavigator({
  // teste: { screen: esqueciSenha },
  Login: { screen: Login },
  cadastro: { screen: cadastro },
  cadastroConfirmacao: { screen: cadastroConfirmacao },
  esqueciSenha: { screen: esqueciSenha },
  EsqueciSenhaConfirmacao: { screen: EsqueciSenhaConfirmacao }
}, {
    transitionConfig: () => defaultTransaction(),  
    headerMode: "none",
  })

HistoricoFaleconoscoStack = createStackNavigator({
  historico: { screen: historico },
  mensagens: { screen: mensagens },
  novoFaleConosco: { screen: enviarMensagem },
}, {
  headerMode: "none",
  navigationOptions: {
    headerStyle: {
      backgroundColor: colors.white,
      borderBottomColor: 'transparent',
    },
    headerTintColor: colors.black,
    headerBackTitle: null,
  },
});

DuvidasFrequentesStack = createStackNavigator({
  duvidasFrequentes: { screen: duvidasFrequentes },
  duvidasFrequentesInterna: { screen: duvidasFrequentesInterna },
}, {
    headerMode: "none",
  });

NotificacaoStack = createStackNavigator({
  notificacao: { screen: notificacao },
  notificacaoInterna: { screen: notificacaoInterna },
}, {
  headerMode: "none",
  navigationOptions: {
    headerStyle: {
      backgroundColor: colors.white,
      borderBottomColor: 'transparent',
    },
    headerTintColor: colors.black,
    headerBackTitle: null,
  },
});


const MainRoute = DrawerNavigator({
  // teste: { screen: Regulamento },
  HomeStackNavigator: { screen: HomeStackNavigator },
  DuvidasFrequentesStack: { screen: DuvidasFrequentesStack },
  meuPerfil: { screen: meuPerfil },
  NotificacaoStack: { screen: NotificacaoStack },
  regulamento: { screen: Regulamento },
  Inicial: { screen: Inicial },
  meuPerfilDadosPessoais: { screen: meuPerfilDadosPessoais },
  meuPerfilSenha: { screen: meuPerfilSenha },
  LojasStack: { screen: LojasStack },
  HistoricoFaleconoscoStack: { screen: HistoricoFaleconoscoStack },
},
{
  
  transitionConfig: () => defaultTransaction(),  
  
  drawerPosition: 'right',
  contentComponent: props => (
    <DrawerSidebar {...props} />
  ),
});

const Routes = createStackNavigator({
  Splash: { screen: Splash },
  tutorial: { screen: Tutorial },
  semInternet: { screen: semInternet },
  LoginStack: { screen: LoginStack },
  esqueciSenha: { screen: esqueciSenha },
  termosUso: { screen: termosUso },
  MainRoute: { screen: MainRoute },
  meuPerfil: { screen: meuPerfil },
  meuPerfilDadosPessoais: { screen: meuPerfilDadosPessoais },
  meuPerfilSenha: { screen: meuPerfilSenha },
}, {
  headerMode: "none",
  navigationOptions: {
    transitionConfig: () => defaultTransaction(),  
    headerStyle: {
      backgroundColor: colors.white,
      borderBottomColor: 'transparent',
    },
    headerTintColor: colors.black,
    headerBackTitle: null,
  },
  headerTintColor: colors.black,
  headerBackTitle: null,
});



const AppContainer = (Routes);

export default AppContainer;
