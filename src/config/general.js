console.disableYellowBox = true;


const URL = 'http://www.pontueme.com.br';
// const URL = 'http://localhost:57475';
//const URL = 'http://138.97.105.194:126';  // homolog

export const appId = {
  name: 'eldorado'
}

export const general = {
  URL,
  codCliente: 1,
  baseURL: URL + '/api',
  appName: 'Conexão Eldorado',
  nomeClienteFaleConosco: 'Conexão Eldorado',
  appId: appId.name,
  Version_Android: '1.5',
  Version_IOS: '1.5',
  imagemPerfil: URL+'/Conteudo/Clientes/'+appId.name+'/associados/',
  imagemLojas: URL+'/conteudo/clientes/'+appId.name+'/Lojas/',
  imagemNotificacoes: URL+'/conteudo/clientes/'+appId.name+'/Mensagens/',
  imagemBeneficio: URL+'/conteudo/clientes/'+appId.name+'/beneficios/',
  imagemBanners: URL+'/conteudo/clientes/'+appId.name+'/banners/',
  imagemMedalha: URL+'/conteudo/clientes/'+appId.name+'/categoriaspontos/',
  IDPagina_Regulamento: 18,
  IDPagina_SobreOPrograma: 19,
  IDPagina_SobreOPrograma_topo: 20,
  IDPagina_SobreOPrograma_recompensas: 15,
  IDPagina_SobreOPrograma_beneficios: 14

};


export default general;

export const UrlLogin = URL + '/api/token';
export const UrlAssociarFacebook = URL + '/';
export const UrlCadastroUsuario = URL + '/Api/Associados/CadastroComSenha';